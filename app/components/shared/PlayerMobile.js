import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Link from 'next/link'
import Image from './Image'
import Loader from './Loader'
import PlayIcon from '../../server/assets/svg/play-icon.svg'
import PauseIcon from '../../server/assets/svg/pause-icon.svg'
import NextIcon from '../../server/assets/svg/next-icon.svg'
import cache from '../../helpers/cache'
import events from '../../helpers/events'
import actions from '../../helpers/actions'
import {
  formatTime,
  getImageSrc,
  getPageUrl,
  getProgressPercent,
  getParentTitle,
} from '../../helpers/audio'
const { UPDATE_PLAYER } = actions

const useStyles = makeStyles(theme => ({
  PLAYER_MOBILE_ROOT: {
    position: 'fixed',
    left: 0,
    right: 0,
    bottom: theme.vars.PAGE_BORDER_HEIGHT,
    height: theme.vars.PLAYER_MOBILE_HEIGHT,
    background: theme.vars.ACCENT_COLOR,
  },
  PLAYER_MOBILE_IMAGE: {
    cursor: 'pointer',
    background: theme.vars.ACCENT_COLOR_GRAY,
    height: theme.vars.PLAYER_MOBILE_HEIGHT,
    width: theme.vars.PLAYER_MOBILE_HEIGHT,
    position: 'absolute',
    top: 0,
    left: 0
  },
  PLAYER_MOBILE_TEXT: {
    cursor: 'pointer',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '0 15px',
    height: theme.vars.PLAYER_MOBILE_HEIGHT,
    width: `calc(100% - ${2.3 * theme.vars.PLAYER_MOBILE_HEIGHT}px)`,
    position: 'absolute',
    top: 0,
    left: theme.vars.PLAYER_MOBILE_HEIGHT
  },
  PLAYER_MOBILE_TEXT_TITLE: {
    color: theme.vars.ACCENT_COLOR_INVERSE,
    fontSize: 16,
    fontWeight: 'bold',
    pointerEvents: 'none',
    ...theme.mixins.ellipsis()
  },
  PLAYER_MOBILE_TEXT_SUBTITLE: {
    color: theme.vars.ACCENT_COLOR_GRAY,
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 5,
    pointerEvents: 'none',
    ...theme.mixins.ellipsis()
  },
  PLAYER_MOBILE_PLAY_PAUSE: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: theme.vars.PLAYER_MOBILE_HEIGHT * 0.5,
    height: theme.vars.PLAYER_MOBILE_HEIGHT,
    position: 'absolute',
    top: 0,
    right: theme.vars.PLAYER_MOBILE_HEIGHT * 0.8,
    '& svg': {
      cursor: 'pointer'
    },
    '& svg path': {
      fill: theme.vars.ACCENT_COLOR_INVERSE
    },
  },
  PLAYER_MOBILE_NEXT: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: theme.vars.PLAYER_MOBILE_HEIGHT,
    width: theme.vars.PLAYER_MOBILE_HEIGHT * 0.8,
    position: 'absolute',
    top: 0,
    right: 0,
    paddingLeft: 15,
    '& svg': {
      cursor: 'pointer'
    },
    '& svg path': {
      fill: theme.vars.ACCENT_COLOR_DARK_GRAY
    }
  },
  PLAYER_MOBILE_LOADER: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: 'auto',
    zIndex: 2,
    height: 0.5 * theme.vars.PLAYER_MOBILE_HEIGHT,
    width: 0.5 * theme.vars.PLAYER_MOBILE_HEIGHT,
    pointerEvents: 'none'
  },
  PLAYER_MOBILE_LOADER_OVERLAY: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 1.3 * theme.vars.PLAYER_MOBILE_HEIGHT,
    opacity: 0.7,
    background: theme.vars.ACCENT_COLOR,
    zIndex: 1,
    pointerEvents: 'none'
  }
}))

const PlayerMobile = () => {
  const classes = useStyles()
  const theme = cache.get('app').getTheme()

  const player = cache.get('player')
  const [buffering, setBuffering] = React.useState(player && player.state.buffering)
  const [fetching, setFetching] = React.useState(player && player.state.fetching)
  const [loading, setLoading] = React.useState(player && player.state.loading)
  const [playing, setPlaying] = React.useState(player && player.state.playing)
  const [seeking, setSeeking] = React.useState(player && player.state.seeking)
  const [error, setError] = React.useState(player && player.state.error)
  const [audio, setAudio] = React.useState(player && player.state.audio)
  const [currentTime, setCurrentTime] = React.useState(player && player.state.currentTime)
  const [totalTime, setTotalTime] = React.useState(player && player.state.totalTime)

  React.useEffect(() => {
    const playerEventKey = events.on(UPDATE_PLAYER, state => {
      setBuffering(state.buffering)
      setFetching(state.fetching)
      setLoading(state.loading)
      setSeeking(state.seeking)
      setPlaying(state.playing)
      setError(state.error)
      setAudio(state.audio)
      setCurrentTime(state.currentTime)
      setTotalTime(state.totalTime)
    })

    return () => events.off(playerEventKey)
  }, [])

  const showLoader = !error && (!audio || fetching || buffering || loading)

  const url = getPageUrl(audio)

  return (
    <div className={classes.PLAYER_MOBILE_ROOT}>

      {/* Overlay */}
      {showLoader && <div className={classes.PLAYER_MOBILE_LOADER_OVERLAY} />}

      {/* Loader */}
      {showLoader && (
        <div className={classes.PLAYER_MOBILE_LOADER}>
          <Loader color={theme.vars.ACCENT_COLOR_INVERSE} />
        </div>
      )}

      {/* Image */}
      {audio && (
        <Link href={url.href} as={url.as}>
          <div className={classes.PLAYER_MOBILE_IMAGE}>
            <Image src={getImageSrc(audio, '?tr=w-100')} />
          </div>
        </Link>
      )}

      {/* Text */}
      {audio && (
        <Link href={url.href} as={url.as}>
          <div className={classes.PLAYER_MOBILE_TEXT}>
            <div className={classes.PLAYER_MOBILE_TEXT_TITLE}>
              {audio.title} - {getParentTitle(audio)}
            </div>
            <div className={classes.PLAYER_MOBILE_TEXT_SUBTITLE}>
              {
                error ? error
                  : loading ? 'Loading...'
                  : seeking ? 'Seeking...'
                  : buffering ? 'Buffering...'
                  : formatTime(currentTime)
              }
            </div>
          </div>
        </Link>
      )}

      {/* Play/Pause */}
      {audio && (
        <div className={classes.PLAYER_MOBILE_PLAY_PAUSE}>
          {
            playing
              ? <PauseIcon width="100%" onClick={() => cache.get('player').pause()} />
              : <PlayIcon width="100%" onClick={() => cache.get('player').play()} />
          }
        </div>
      )}

      {/* Next */}
      {audio && (
        <div className={classes.PLAYER_MOBILE_NEXT}>
          <NextIcon width="48%" onClick={() => cache.get('player').next()} />
        </div>
      )}

    </div>
  )
}

PlayerMobile.defaultProps = {}

export default PlayerMobile
