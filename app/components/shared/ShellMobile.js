import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import PageBackground from './PageBackground'
import PageBorder from './PageBorder'
import PlayerMobile from './PlayerMobile'
import HeaderMobile from './HeaderMobile'
import SearchMobile from './SearchMobile'

const useStyles = makeStyles(theme => ({
  SHELL_MOBILE_ROOT: {
    position: 'absolute',
    top: theme.vars.PAGE_BORDER_HEIGHT,
    left: 0,
    right: 0,
    background: 'white',
    minHeight: `calc(100% - ${2 * theme.vars.PAGE_BORDER_HEIGHT}px)`,
  },
  SHELL_MOBILE_CONTENT: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: theme.vars.HEADER_MOBILE_HEIGHT,
    paddingBottom: theme.vars.PLAYER_MOBILE_HEIGHT + theme.vars.PAGE_BORDER_HEIGHT,
    position: 'absolute',
    minHeight: '100%',
    width: '100%',
  },
}))

const ShellMobile = ({ children, className, contentClassName, header, player }) => {
  const classes = useStyles()

  return (
    <div className={[classes.SHELL_MOBILE_ROOT, className].join(' ')}>
      <PageBackground />
      <PageBorder top />
      {header && <HeaderMobile />}
      <div className={[classes.SHELL_MOBILE_CONTENT, contentClassName].join(' ')}>
        { children }
      </div>
      {player && <PlayerMobile />}
      <PageBorder bottom />
    </div>
  )
}

ShellMobile.defaultProps = {
  children: null,
  className: '',
  contentClassName: '',
  header: true,
  player: true
}

export default ShellMobile
