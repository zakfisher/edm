import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Head from './Head'
import isClient from '../../helpers/is-client'
import cache from '../../helpers/cache'

const useStyles = makeStyles(theme => ({
  PAGE_ROOT: {
    minHeight: '100%',
  },
}))

const isMobile = () => isClient && window.innerWidth <= 960 // md

const Page = ({
  href,
  head,
  Mobile,
  Desktop
}) => {
  const classes = useStyles()

  const [mobile, setMobile] = React.useState(isMobile());
  const resize = () => setMobile(isMobile())
  React.useEffect(() => {
    window.addEventListener('resize', resize)
    return () => window.removeEventListener('resize', resize)
  }, [])

  const theme = cache.get('app').getTheme()

  return (
    <div className={classes.PAGE_ROOT}>
      <Head {...head} />

      <style jsx global>
        {`
          ::selection {
            background: ${theme.vars.ACCENT_COLOR_INVERSE};
            color: ${theme.vars.ACCENT_COLOR};
          }
        `}
      </style>

      {
        mobile
          ? typeof Mobile === 'function'
            ? Mobile() : <Mobile />
          : typeof Desktop === 'function'
            ? Desktop() : <Desktop />
      }
    </div>
  )
}

Page.defaultProps = {
  href: '/',
  head: {},
  topBorder: false,
  bottomBorder: false,
  Mobile: null,
  Desktop: null,
}

export default Page
