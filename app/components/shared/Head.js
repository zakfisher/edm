import React from 'react'
import NextHead from 'next/head'
import { string } from 'prop-types'
import cache from '../../helpers/cache'
import events from '../../helpers/events'
import actions from '../../helpers/actions'
import {
  getParentTitle
} from '../../helpers/audio'

const { UPDATE_PLAYER } = actions

let playerEventKey = null

const Head = props => {

  const [title, setTitle] = React.useState(props.title)

  // Bind to Player
  if (playerEventKey) events.off(playerEventKey)
  playerEventKey = events.on(UPDATE_PLAYER, ({ audio }) => {
    if (!audio) return
    if (title.includes(audio.title)) return
    setTitle(props.title + ' - ' + audio.title + ' - ' + getParentTitle(audio))
  })

  return (
    <NextHead>
      <title>{title}</title>
      {/*<link rel="icon" sizes="192x192" href="/assets/touch-icon.png" />*/}
      {/*<link rel="apple-touch-icon" href="/assets/touch-icon.png" />*/}
      {/*<link rel="mask-icon" href="/assets/favicon-mask.svg" color="#49B882" />*/}
      <meta name="keywords" content={props.keywords} />
      <meta name="description" content={props.description} />
      <meta property="og:url" content={props.url} />
      <meta property="og:title" content={props.title} />
      <meta property="og:description" content={props.description} />
      <meta name="twitter:site" content={props.url} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:image" content={props.ogImage} />
      <meta property="og:image" content={props.ogImage} />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="630" />
    </NextHead>
  )
}

Head.defaultProps = {
  title: 'EDM Town',
  description: 'Where all your dreams come true',
  keywords: 'edm,house,music',
  url: 'edm.town',
  ogImage: ''
}

Head.propTypes = {
  title: string,
  description: string,
  keywords: string,
  url: string,
  ogImage: string
}

export default Head
