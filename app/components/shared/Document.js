import React, { Fragment } from 'react'
import NextDocument, { Head, Main, NextScript } from 'next/document'
import { ServerStyleSheets } from '@material-ui/styles'
import isDev from '../../helpers/is-dev'

class Document extends NextDocument {
  render() {
    return (
      <html lang="en">
        <Head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no" />
          <meta httpEquiv="ScreenOrientation" content="autoRotate:disabled" />
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
          <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
          <script src="/assets/gsap/src/minified/TweenMax.min.js" />
          <script src="/assets/gsap/src/minified/plugins/ScrollToPlugin.min.js" />
          <script src="/assets/scrollmagic/ScrollMagic.min.js" />
          { isDev ? <script src="/assets/scrollmagic/plugins/debug.addIndicators.min.js" /> : null }
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}

// Next.js + Material UI Setup @
// https://github.com/mui-org/material-ui/blob/master/examples/nextjs/pages/_document.js#L86
Document.getInitialProps = async context => {
  const sheets = new ServerStyleSheets()
  const { renderPage, req } = context
  const originalRenderPage = renderPage
  context.renderPage = () => {
    const enhanceApp = App => props => sheets.collect(<App {...props} />)
    return originalRenderPage({ enhanceApp })
  }
  const initialProps = await NextDocument.getInitialProps(context)
  const APP = {
    env: req.get('Host').includes('localhost:') ? 'local' : 'production'
  }
  const styles = [
    <Fragment key="injections">
      {initialProps.styles}
      {sheets.getStyleElement()}
    </Fragment>,
    <style key="global-styles" dangerouslySetInnerHTML={{__html: `
      * { position: relative; }
      html, body { background-color: #000 !important; }
      html, body, body > div { height: 100%; }
    `}} />,
    <script key="APP" type="text/javascript" dangerouslySetInnerHTML={{
      __html: `window.APP = ${JSON.stringify(APP)};`
    }} />
  ]
  return { ...initialProps, styles }
}

export default Document
