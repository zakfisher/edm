import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Image from './Image'
import cache from '../../helpers/cache'

const useStyles = makeStyles(theme => ({
  PAGE_BACKGROUND_ROOT: {
    position: 'fixed',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex: 0,
    background: 'black'
  },
  PAGE_BACKGROUND_GRADIENT: {
    background: theme.vars.GRADIENT_V,
    position: 'absolute',
    height: '100%',
    width: '100%',
    opacity: 0.93,
    zIndex: 1
  }
}))

const PageBackground = ({ top, bottom }) => {
  const classes = useStyles()
  const imageUrl = cache.get('app').getTheme().vars.BG_IMAGE_1

  return (
    <div className={classes.PAGE_BACKGROUND_ROOT}>
      <Image src={imageUrl} />
      <div className={classes.PAGE_BACKGROUND_GRADIENT} />
    </div>
  )
}

PageBackground.defaultProps = {}

export default PageBackground
