import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  PAGE_BORDER_TOP: {
    background: theme.vars.ACCENT_COLOR,
    height: theme.vars.PAGE_BORDER_HEIGHT,
    position: 'fixed',
    left: 0,
    right: 0,
    top: 0,
    zIndex: 10
  },
  PAGE_BORDER_BOTTOM: {
    background: theme.vars.GRADIENT_H,
    height: theme.vars.PAGE_BORDER_HEIGHT,
    position: 'fixed',
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 10
  },
}))

const PageBorder = ({ top, bottom }) => {
  const classes = useStyles()
  if (top) return <div className={classes.PAGE_BORDER_TOP} />
  if (bottom) return <div className={classes.PAGE_BORDER_BOTTOM} />
  return null
}

PageBorder.defaultProps = {
  bottom: false,
  top: false,
}

export default PageBorder
