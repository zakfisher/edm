import React from 'react'
import _ from 'lodash'
import Router from 'next/router'
import { getAudioUrl, getRandomAudio } from '../../helpers/api'
import cache from '../../helpers/cache'
import events from '../../helpers/events'
import actions from '../../helpers/actions'
import { getPageUrl } from '../../helpers/audio'
import router from 'next/router'
const { UPDATE_PLAYER, PLAYER_READY } = actions

class Player extends React.Component {
  constructor() {
    super()
    this.state = {
      error: null,

      shuffle: true,
      loop: false,

      buffering: false,
      fetching: false,
      loading: false,
      seeking: false,
      playing: false,

      progress: 0,
      currentTime: 0,
      totalTime: 0,

      volume: 100,
      muted: false,

      audio: null,
      podcast: null,
      event: null,
    }
    this.playing = false
    this.history = []
    this.maxHistoryItems = 100
    this.historyIndex = -1
    cache.set('player', this)
  }

  componentDidMount() {
    this.addEvents()
    events.dispatch(PLAYER_READY, this.state)

    // Set initial audio (default audio from page)
    if (router.pathname === '/audio') return

    // Set initial audio (random)
    this.shuffle()
  }

  componentDidUpdate() {
    events.dispatch(UPDATE_PLAYER, this.state)
  }

  addEvents = () => {
    const el = this.refs.audio

    this.addKeyboardEvents()

    el.addEventListener('ended', () => {
      this.next()
    })

    el.addEventListener('canplay', () => {
      this.setState({
        error: null,
        buffering: false,
        fetching: false,
        loading: false,
        seeking: false,
      })
      if (!this.state.playing) return
      el.play()
    })

    el.addEventListener('error', e => {
      this.pause()
      window.requestAnimationFrame(() => {
        this.setState({
          buffering: false,
          fetching: false,
          seeking: false,
          playing: false,
          error: typeof e === 'string' ? e
            : _.get(e, 'target.error.message', 'An error occurred')
        })
      })
    })

    el.addEventListener('seeking', () => {
      this.setState({ seeking: true })
    })

    el.addEventListener('seeked', () => {
      this.setState({ seeking: false })
    })

    el.addEventListener('loadstart', () => {
      this.setState({ loading: true })
    })

    el.addEventListener('loadeddata', () => {
      this.setState({ loading: false })
    })

    el.addEventListener('waiting', () => {
      this.setState({ buffering: true })
    })

    el.addEventListener('durationchange', () => {
      this.setState({ totalTime: el.duration })
    })

    el.addEventListener('timeupdate', () => {
      const isZero = el.currentTime < 1 || el.duration < 1
      this.setState({
        progress: isZero ? 0 : (el.currentTime / el.duration),
        currentTime: isZero ? 0 : el.currentTime
      })
    })
  }

  addKeyboardEvents = () => {
    const keys = {}

    // SPACE - play/pause
    keys[32] = () => this[this.state.playing ? 'pause' : 'play']()

    // RIGHT ARROW - next
    keys[39] = () => this.next()

    // LEFT ARROW - prev
    keys[37] = () => this.prev()

    // M - toggle mute
    keys[77] = () => this[this.state.muted ? 'unmute' : 'mute']()

    // S - toggle shuffle
    keys[83] = () => this.setShuffle(!this.state.shuffle)

    // H - home page
    keys[72] = () => Router.push('/')

    // P - now playing page
    keys[80] = () => {
      const url = getPageUrl(this.state.audio)
      if (!url) return
      Router.push(url.href, url.as)
    }

    // F - search
    keys[70] = () => {
      const params = [
        `query=${cache.get('search-query') || ''}`,
        `tab=${cache.get('search-tab') || 'mixes'}`
      ]
      Router.push(`/search?${params.join('&')}`)
    }

    // MINUS - decrease volume
    keys[189] = () => this.decreaseVolume(10)

    // PLUS - increase volume
    keys[187] = () => this.increaseVolume(10)

    // LEFT SQUARE BRACKET - seek back
    keys[219] = () => this.seekBack(300)

    // RIGHT SQUARE BRACKET - seek forward
    keys[221] = () => this.seekForward(300)

    this.handleKeyboardEvents = e => {
      // console.log(e.keyCode)
      const fn = keys[e.keyCode] || null
      if (!fn) return
      e.preventDefault()
      fn()
    }
    window.document.addEventListener('keydown', this.handleKeyboardEvents)
  }

  removeKeyboardEvents = () => {
    window.document.removeEventListener('keydown', this.handleKeyboardEvents)
  }

  next = () => {
    const endOfHistory = this.historyIndex === this.history.length - 1
    if (endOfHistory) {
      // If shuffle, get random audio
      if (this.state.shuffle) {
        this.shuffle()
      }
      // If not shuffle, play next in podcast/event
      else {
        console.log('get next audio in group')
      }
    }
    else {
      const nextAudio = this.history[this.historyIndex + 1] || null
      if (nextAudio) {
        this.historyIndex++
        this.setAudio(nextAudio, false)
      }
    }
  }

  prev = () => {
    if (this.historyIndex < 1) return
    this.historyIndex--
    const prevAudio = this.history[this.historyIndex] || null
    if (prevAudio) return this.setAudio(prevAudio, false)
  }

  pause = () => {
    if (!this.state.playing) return
    if (!this.state.audio) return
    this.setState({ playing: false })
    const el = this.refs.audio
    if (el && !this.state.buffering && !this.state.fetching) {
      el.pause()
    }
  }

  play = () => {
    if (this.state.playing) return
    if (!this.state.audio) return
    this.setState({ playing: true })
    const el = this.refs.audio
    if (el && !this.state.buffering && !this.state.fetching) {
      el.play()
    }
  }

  seek = (progress = 1) => {
    if (!this.state.audio) return
    const el = this.refs.audio
    if (isNaN(el.duration) || el.duration === 0) return
    el.currentTime = progress * el.duration
  }

  seekBack = (increment = 0) => {
    if (!this.state.audio) return
    const el = this.refs.audio
    if (el.currentTime === 0) return
    let nextTime = el.currentTime - increment
    if (nextTime < 0) nextTime = 0
    el.currentTime = nextTime
  }

  seekForward = (increment = 0) => {
    if (!this.state.audio) return
    const el = this.refs.audio
    if (el.currentTime === el.duration) return
    let nextTime = el.currentTime + increment
    if (nextTime > el.duration) nextTime = el.duration
    el.currentTime = nextTime
  }

  setAudio = async (audio = {}, addToHistory = true) => {
    const el = this.refs.audio
    el.currentTime = 0

    this.setState({
      error: null,
      fetching: true,
      progress: 0,
      currentTime: 0,
      totalTime: 0,
      audio
    })

    let url = null
    const audioId = _.get(audio, '_id', null)
    try { url = await getAudioUrl(audioId) }
    catch(e) { console.error(e) }

    this.setState({
      fetching: false,
      error: !url && 'URL not found'
    })

    if (!url) return

    if (addToHistory) {
      this.history.push(audio)
      this.history = this.history.slice(-this.maxHistoryItems)
      if (this.historyIndex < this.maxHistoryItems - 1) {
        this.historyIndex++
      }
    }

    // Load next track
    el.src = url
    el.load()
  }

  setVolume = (volume = 100) => {
    const el = this.refs.audio
    el.volume = volume > 0 ? (volume / 100).toFixed(2) : 0
    const muted = volume === 0
    el.muted = muted
    this.setState({ volume, muted })
  }

  setShuffle = (shuffle = true) => {
    this.setState({ shuffle })
  }

  shuffle = async (autoPlay = false) => {
    if (this.state.fetching) return
    this.setState({ fetching: true })
    const audio = await getRandomAudio()
    if (audio) this.setAudio(audio)
    else this.setState({ fetching: false })
  }

  decreaseVolume = (increment = 0) => {
    const el = this.refs.audio
    const percent = 100 * el.volume
    let nextVolume = percent - increment
    if (nextVolume < 0) nextVolume = 0
    this.setVolume(nextVolume)
  }

  increaseVolume = (increment = 0) => {
    const el = this.refs.audio
    const percent = 100 * el.volume
    let nextVolume = percent + increment
    if (nextVolume > 100) nextVolume = 100
    this.setVolume(nextVolume)
  }

  mute = () => {
    const el = this.refs.audio
    this.prevVolume = this.state.volume
    this.setVolume(0)
  }

  unmute = () => {
    this.setVolume(this.prevVolume || 100)
  }

  render() {
    return <audio hidden ref="audio" preload="none" />
  }
}

export default Player
