import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import ShellMobile from './ShellMobile'
import Button from './Button'
import Logo from '../../server/assets/svg/logo.svg'

const useStyles = makeStyles(theme => ({
  HOME_MOBILE_ROOT: {},
  HOME_MOBILE_CONTENT: {
    justifyContent: 'space-around',
  },
  HOME_MOBILE_LOGO: {
    '& svg path': {
      fill: theme.vars.ACCENT_COLOR
    }
  },
  HOME_MOBILE_LOGIN: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 3.25 * theme.vars.BUTTON_HEIGHT,
    margin: '20px auto'
  },
  HOME_MOBILE_LOGIN_INFO: {
    color: theme.vars.ACCENT_COLOR,
    fontWeight: 'bold',
    fontSize: 14,
    display: 'inline-block',
    margin: 0,
    cursor: 'pointer',
    paddingBottom: 2,
    borderBottom: `2px solid ${theme.vars.ACCENT_COLOR}`,
    transition: 'background 300ms ease',
    background: 'transparent',
    '&:hover': theme.mixins.hover({
      background: theme.vars.BUTTON_HOVER_BG_COLOR,
    })
  }
}))

const HomeMobile = () => {
  const classes = useStyles()
  return (
    <ShellMobile
      className={classes.HOME_MOBILE_ROOT}
      contentClassName={classes.HOME_MOBILE_CONTENT}
    >
      <div className={classes.HOME_MOBILE_LOGO}>
        <Logo />
      </div>
      <div className={classes.HOME_MOBILE_LOGIN}>
        <Button width={280}>
          Log in with Facebook
        </Button>
        <Button width={280}>
          Log in with Google
        </Button>
        <p className={classes.HOME_MOBILE_LOGIN_INFO}>
          Why should I log in?
        </p>
      </div>
    </ShellMobile>
  )
}

HomeMobile.defaultProps = {}

export default HomeMobile
