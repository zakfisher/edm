import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Router from 'next/router'
import Link from 'next/link'

const useStyles = makeStyles(theme => ({
  BUTTON_ROOT: {
    height: theme.vars.BUTTON_HEIGHT,
    border: `2px solid ${theme.vars.ACCENT_COLOR}`,
    borderRadius: theme.vars.BUTTON_HEIGHT / 2,
    padding: '0 30px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',
    fontSize: 16,
    fontWeight: 'bold',
    overflow: 'hidden',
    transition: 'background 300ms ease',
    '&[data-fill="false"]': {
      background: 'transparent',
      color: theme.vars.ACCENT_COLOR,
      '&:hover': theme.mixins.hover({
        background: theme.vars.BUTTON_HOVER_BG_COLOR
      }),
    },
    '&[data-fill="true"]': {
      background: theme.vars.ACCENT_COLOR,
      color: theme.vars.ACCENT_COLOR,
      '&:hover': theme.mixins.hover({
        background: theme.vars.BUTTON_HOVER_BG_COLOR_INVERSE
      })
    },
  },
}))

const Button = props => {
  const classes = useStyles()
  const [fill, setFill] = React.useState('false')
  React.useEffect(() => {
    setFill(props.fill === true ? 'true' : 'false')
  }, [props.fill])
  return (
    <div className={classes.BUTTON_ROOT}
      data-fill={fill}
      onClick={props.onClick}
      style={{ width: props.width }}
    >
      {props.children}
    </div>
  )
}

Button.defaultProps = {
  children: 'Click Me!',
  fill: null,
  width: 'auto',
  onClick: () => null
}

export default Button
