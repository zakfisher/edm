import React from 'react'
import cache from '../../helpers/cache'
import events from '../../helpers/events'
import actions from '../../helpers/actions'
import { getAudioById } from '../../helpers/api'
const { UPDATE_PLAYER } = actions

let playerEventKey = null

const PlayerDemo = () => {
  const [currentTime, setCurrentTime] = React.useState(0)
  const [totalTime, setTotalTime] = React.useState(0)
  const [volume, setVolume] = React.useState(0)
  const [muted, setMuted] = React.useState(false)
  const [progress, setProgress] = React.useState(false)
  const [error, setError] = React.useState(null)
  const [loading, setLoading] = React.useState(false)
  const [playing, setPlaying] = React.useState(false)
  const [audio, setAudio] = React.useState(false)
  const [podcast, setPodcast] = React.useState(false)
  const [event, setEvent] = React.useState(false)

  // Bind to Player
  if (playerEventKey) events.off(playerEventKey)
  playerEventKey = events.on(UPDATE_PLAYER, state => {
    setCurrentTime(state.currentTime)
    setTotalTime(state.totalTime)
    setVolume(state.volume)
    setMuted(state.muted)
    setProgress(state.progress)
    setError(state.error)
    setLoading(state.loading)
    setPlaying(state.playing)
    setAudio(state.audio)
    setPodcast(state.podcast)
    setEvent(state.event)
  })

  return (
    <div>
      <a onClick={async () => {
        const player = cache.get('player')
        if (playing) return player.pause()
        else if (player.state.audio) return player.play()
        const audio = await getAudioById('5d7ae9cdfbc2ba850bc632a9')
        player.setAudio(audio)
      }}>{playing ? 'Pause' : 'Play'}</a>
      <p>Audio: {audio && audio.title}</p>
      <p>Podcast: {podcast && podcast.title}</p>
      <p>Event: {event && event.title}</p>
      <p>Progress: {progress}</p>
      <p>Current Time: {currentTime}</p>
      <p>Total Time: {totalTime}</p>
      <p>Volume: {volume}</p>
      <p>Muted: {muted.toString()}</p>
      <p>Playing: {playing.toString()}</p>
      <p>Loading: {loading.toString()}</p>
      <p>Error: {error}</p>
    </div>
  )
}

export default PlayerDemo
