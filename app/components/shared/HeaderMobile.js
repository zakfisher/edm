import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Link from 'next/link'
import Router from 'next/router'
import LogoIcon from '../../server/assets/svg/logo-icon.svg'
import SearchIcon from '../../server/assets/svg/search-icon.svg'
import MenuIcon from '../../server/assets/svg/menu-icon.svg'
import cache from '../../helpers/cache'

const useStyles = makeStyles(theme => ({
  HEADER_MOBILE_ROOT: {
    position: 'fixed',
    top: theme.vars.PAGE_BORDER_HEIGHT,
    left: 0,
    right: 0,
    height: theme.vars.HEADER_MOBILE_HEIGHT,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '0 20px',
    zIndex: 2,
  },
  HEADER_MOBILE_LOGO: {
    '& svg': {
      cursor: 'pointer'
    },
    '& svg path': {
      fill: theme.vars.ACCENT_COLOR
    }
  },
  HEADER_MOBILE_SEARCH: {
    width: `calc(100% - ${20 + theme.vars.HEADER_MOBILE_HEIGHT}px)`,
    maxWidth: 400,
    '& input': {
      border: `2px solid ${theme.vars.ACCENT_COLOR}`,
      borderRadius: theme.vars.INPUT_HEIGHT / 2,
      height: theme.vars.INPUT_HEIGHT,
      outline: 'none',
      width: '100%',
      padding: '0 15px',
      fontSize: 16,
      fontWeight: 'bold',
      cursor: 'text',
      color: theme.vars.ACCENT_COLOR,
      textShadow: 'none',
      boxShadow: 'none',
      '&::placeholder': {
        color: theme.vars.ACCENT_COLOR,
        fontSize: 16,
      },
      background: 'transparent',
      transition: 'background 300ms ease',
      '&:hover': theme.mixins.hover({
        background: theme.vars.INPUT_HOVER_BG_COLOR
      })
    },
    '& svg': {
      position: 'absolute',
      right: 15,
      top: 0,
      bottom: 0,
      margin: 'auto',
    },
    '& svg path': {
      fill: theme.vars.ACCENT_COLOR
    }
  },
  HEADER_MOBILE_MENU: {
    '& svg': {
      cursor: 'pointer'
    },
    '& svg path': {
      fill: theme.vars.ACCENT_COLOR
    }
  },
}))

const MobileHeader = () => {
  const classes = useStyles()
  const query = cache.get('search-query') || ''
  return (
    <header className={classes.HEADER_MOBILE_ROOT}>
      <Link href="/">
        <div className={classes.HEADER_MOBILE_LOGO}>
          <LogoIcon />
        </div>
      </Link>
      <div className={classes.HEADER_MOBILE_SEARCH}>
        <input
          readOnly
          type="text"
          placeholder="Search"
          onFocus={() => {
            const params = [
              `query=${query}`,
              `tab=${cache.get('search-tab') || 'mixes'}`
            ]
            Router.push(`/search?${params.join('&')}`)
          }}
          value={query}
        />
        <SearchIcon />
      </div>
      <div className={classes.HEADER_MOBILE_MENU}>
        <MenuIcon />
      </div>
    </header>
  )
}

MobileHeader.defaultProps = {}

export default MobileHeader
