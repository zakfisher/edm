import React from 'react'
import _ from 'lodash'
import { makeStyles } from '@material-ui/core/styles'
import ShellMobile from './ShellMobile'
import Image from './Image'
import Loader from './Loader'
import Button from './Button'
import cache from '../../helpers/cache'
import events from '../../helpers/events'
import actions from '../../helpers/actions'
import Router from 'next/router'
import {
  formatTime,
  getImageSrc,
  getPageUrl,
  getProgressPercent,
  getParentTitle,
} from '../../helpers/audio'
const { UPDATE_PLAYER } = actions
import PrevIcon from '../../server/assets/svg/prev-icon.svg'
import SkipBackwardIcon from '../../server/assets/svg/skip-backward-icon.svg'
import PlayIcon from '../../server/assets/svg/play-icon.svg'
import PauseIcon from '../../server/assets/svg/pause-icon.svg'
import SkipForwardIcon from '../../server/assets/svg/skip-forward-icon.svg'
import NextIcon from '../../server/assets/svg/next-icon.svg'
import ShuffleIcon from '../../server/assets/svg/shuffle-icon.svg'
import StarIcon from '../../server/assets/svg/star-icon.svg'
import MuteIcon from '../../server/assets/svg/mute-icon.svg'
import VolumeIcon from '../../server/assets/svg/volume-icon.svg'
import isTouchScreen from '../../helpers/is-touch-screen'

const useStyles = makeStyles(theme => ({
  AUDIO_MOBILE_ROOT: {},
  AUDIO_MOBILE_CONTENT: {
    justifyContent: 'space-around'
  },
  AUDIO_MOBILE_LOADER: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: 'auto',
    zIndex: 2,
    height: 0.5 * theme.vars.PLAYER_MOBILE_HEIGHT,
    width: 0.5 * theme.vars.PLAYER_MOBILE_HEIGHT,
    pointerEvents: 'none',
    transform: 'translateY(-100%)'
  },
  AUDIO_MOBILE_LOADER_TEXT: {
    color: theme.vars.ACCENT_COLOR,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  AUDIO_MOBILE_IMAGE: {
    // border: '1px solid red',
    height: theme.vars.AUDIO_MOBILE_IMAGE_WIDTH,
    width: theme.vars.AUDIO_MOBILE_IMAGE_WIDTH,
  },
  AUDIO_MOBILE_TEXT: {
    // border: '1px solid red',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    minHeight: 120,
    width: '100%'
  },
  AUDIO_MOBILE_TEXT_TITLE: {
    color: theme.vars.ACCENT_COLOR,
    fontSize: 20,
    fontWeight: 'bold',
    width: '90%',
    marginBottom: 5
  },
  AUDIO_MOBILE_TEXT_SUBTITLE: {
    // border: '1px solid red',
    color: theme.vars.ACCENT_COLOR,
    fontWeight: 'bold',
    fontSize: 14,
    display: 'inline-block',
    margin: 0,
    maxWidth: '100%',
    '& p': {
      ...theme.mixins.ellipsis(),
      background: 'transparent',
      cursor: 'pointer',
      paddingBottom: 2,
      borderBottom: `2px solid ${theme.vars.ACCENT_COLOR}`,
      transition: 'background 300ms ease',
      margin: 0,
      '&:hover': theme.mixins.hover({
        background: theme.vars.BUTTON_HOVER_BG_COLOR,
      })
    }
  },
  AUDIO_MOBILE_PRIMARY_CONTROLS: {
    // border: '1px solid red',
    height: 65,
    width: '100%',
    maxWidth: 600,
    display: 'flex',
    justifyContent: 'center',
    '& > div': {
      // border: '1px solid red',
      width: '20%',
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& svg path': {
        cursor: 'pointer',
        fill: theme.vars.ACCENT_COLOR,
      }
    }
  },
  AUDIO_MOBILE_PRIMARY_CONTROLS_PLAY_PAUSE: {
    '& svg path': {
      fill: theme.vars.ACCENT_COLOR_INVERSE + ' !important',
      stroke: theme.vars.ACCENT_COLOR,
      strokeWidth: 1.25
    }
  },
  AUDIO_MOBILE_SECONDARY_CONTROLS: {
    // border: '1px solid red',
    height: theme.vars.BUTTON_HEIGHT + 30,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    maxWidth: 600,
    '& > div': {
      // border: '1px solid red',
      width: '33.33%',
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& svg path': {
        transition: 'fill 300ms ease',
        fill: theme.vars.ACCENT_COLOR
      },
      '& [data-fill="true"] svg path': {
        fill: theme.vars.ACCENT_COLOR_INVERSE
      },
    }
  },
  AUDIO_MOBILE_TIMELINE: {
    // border: '1px solid red',
    position: 'fixed',
    left: 0,
    right: 0,
    bottom: theme.vars.PAGE_BORDER_HEIGHT,
    height: theme.vars.PLAYER_MOBILE_HEIGHT,
  },
  AUDIO_MOBILE_TIMELINE_BG: {
    background: theme.vars.ACCENT_COLOR,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 0.4,
    cursor: 'pointer'
  },
  AUDIO_MOBILE_TIMELINE_PROGRESS: {
    background: theme.vars.ACCENT_COLOR,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    width: '50%',
    pointerEvents: 'none'
  },
  AUDIO_MOBILE_TIMELINE_TIME: {
    color: theme.vars.ACCENT_COLOR_INVERSE,
    fontSize: 14,
    fontWeight: 'bold',
    position: 'absolute',
    top: 0,
    bottom: 0,
    margin: 'auto',
    display: 'flex',
    alignItems: 'center',
    pointerEvents: 'none'
  },
  AUDIO_MOBILE_TIMELINE_CURRENT_TIME: {
    left: 20,
  },
  AUDIO_MOBILE_TIMELINE_TOTAL_TIME: {
    right: 20,
  },
  AUDIO_MOBILE_TIMELINE_LOADER: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: 'auto',
    zIndex: 2,
    height: 0.5 * theme.vars.PLAYER_MOBILE_HEIGHT,
    width: 0.5 * theme.vars.PLAYER_MOBILE_HEIGHT,
    pointerEvents: 'none'
  },
  AUDIO_MOBILE_TIMELINE_LOADER_OVERLAY: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    opacity: 0.7,
    background: theme.vars.ACCENT_COLOR,
    zIndex: 1,
  },
  AUDIO_MOBILE_TIMELINE_SCRUBBER: {
    position: 'absolute',
    width: 1,
    height: '100%',
    background: theme.vars.ACCENT_COLOR_INVERSE,
    zIndex: 2,
    pointerEvents: 'none'
  }
}))

const AudioMobile = ({ audio }) => {
  const classes = useStyles()
  const theme = cache.get('app').getTheme()

  const player = cache.get('player')
  const [buffering, setBuffering] = React.useState(player && player.state.buffering)
  const [fetching, setFetching] = React.useState(player && player.state.fetching)
  const [loading, setLoading] = React.useState(player && player.state.loading)
  const [playing, setPlaying] = React.useState(player && player.state.playing)
  const [seeking, setSeeking] = React.useState(player && player.state.seeking)
  const [error, setError] = React.useState(player && player.state.error)
  const [muted, setMuted] = React.useState(player && player.state.muted)
  const [shuffle, setShuffle] = React.useState(player && player.state.shuffle)
  const [progress, setProgress] = React.useState(player && player.state.progress)
  const [currentTime, setCurrentTime] = React.useState(player && player.state.currentTime)
  const [totalTime, setTotalTime] = React.useState(player && player.state.totalTime)
  const [timelineHover, setTimelineHover] = React.useState(false)
  const [scrubberPercent, setScrubberPercent] = React.useState(false)

  // Force this page to always show what's currently playing
  const isCurrentlyPlaying = audio && (_.get(player, 'state.audio._id', null) === audio._id)
  if (audio && player && player.state.audio && !isCurrentlyPlaying) {
    const url = getPageUrl(player.state.audio)
    Router.replace(url.href, url.as)
  }

  React.useEffect(() => {
    const playerEventKey = events.on(UPDATE_PLAYER, state => {
      setBuffering(state.buffering)
      setFetching(state.fetching)
      setLoading(state.loading)
      setSeeking(state.seeking)
      setPlaying(state.playing)
      setMuted(state.muted)
      setShuffle(state.shuffle)
      setError(state.error)
      setProgress(state.progress)
      setCurrentTime(state.currentTime)
      setTotalTime(state.totalTime)
    })

    return () => events.off(playerEventKey)
  }, [])

  const showLoader = !error && (fetching || buffering || loading)

  return (
    <ShellMobile
      className={classes.AUDIO_MOBILE_ROOT}
      contentClassName={classes.AUDIO_MOBILE_CONTENT}
      player={audio && !isCurrentlyPlaying}
    >

      {/* Loader */}
      {!audio && (
        <div className={classes.AUDIO_MOBILE_LOADER}>
          <Loader color={theme.vars.ACCENT_COLOR} />
        </div>
      )}

      {/* Loader Text */}
      {!audio && (
        <p className={classes.AUDIO_MOBILE_LOADER_TEXT}>
          Waiting for the drop...
        </p>
      )}

      {/* Image */}
      {audio && (
        <div className={classes.AUDIO_MOBILE_IMAGE}>
          <Image src={getImageSrc(audio, `?tr=w-${theme.vars.AUDIO_MOBILE_IMAGE_WIDTH}`)} />
        </div>
      )}

      {/* Text */}
      {audio && (
        <div className={classes.AUDIO_MOBILE_TEXT}>
          <div className={classes.AUDIO_MOBILE_TEXT_TITLE}>
            {audio.title}
          </div>
          <div className={classes.AUDIO_MOBILE_TEXT_SUBTITLE}>
            <p>{getParentTitle(audio)}</p>
          </div>
        </div>
      )}

      {/* Primary Controls */}
      {audio && (
        <div className={classes.AUDIO_MOBILE_PRIMARY_CONTROLS}>
          <div className={classes.AUDIO_MOBILE_PRIMARY_CONTROLS_PREV_TRACK}>
            <PrevIcon height={25} onClick={() => cache.get('player').prev()} />
          </div>
          <div className={classes.AUDIO_MOBILE_PRIMARY_CONTROLS_SEEK_BACKWARD}>
            <SkipBackwardIcon height={25} onClick={() => cache.get('player').seekBack(300)} />
          </div>
          <div className={classes.AUDIO_MOBILE_PRIMARY_CONTROLS_PLAY_PAUSE}>
            {
              isCurrentlyPlaying && playing
                ? <PauseIcon height={55} width="100%" onClick={() => cache.get('player').pause()} />
                : <PlayIcon height={65} width="100%" onClick={() => {
                  if (!isCurrentlyPlaying) {
                    cache.get('player').setAudio(audio)
                  }
                  cache.get('player').play()
                }}/>
            }
          </div>
          <div className={classes.AUDIO_MOBILE_PRIMARY_CONTROLS_SEEK_FORWARD}>
            <SkipForwardIcon height={25} onClick={() => cache.get('player').seekForward(300)} />
          </div>
          <div className={classes.AUDIO_MOBILE_PRIMARY_CONTROLS_NEXT_TRACK}>
            <NextIcon height={25} onClick={() => cache.get('player').next()} />
          </div>
        </div>
      )}

      {/* Secondary Controls */}
      {audio && (
        <div className={classes.AUDIO_MOBILE_SECONDARY_CONTROLS}>
          <div className={classes.AUDIO_MOBILE_SECONDARY_CONTROLS_SHUFFLE_TOGGLE}>
            <Button
              width={100}
              fill={shuffle}
              onClick={() => cache.get('player').setShuffle(!shuffle)}>
              <ShuffleIcon height={50} />
            </Button>
          </div>
          <div className={classes.AUDIO_MOBILE_SECONDARY_CONTROLS_FAVORITE_TOGGLE}>
            <Button width={100}>
              <StarIcon height="100%" />
            </Button>
          </div>
          <div className={classes.AUDIO_MOBILE_SECONDARY_CONTROLS_MUTE_TOGGLE}>
            <Button
              fill={muted}
              width={100} onClick={() => cache.get('player')[muted ? 'unmute' : 'mute']()}
            >
              { muted ? <VolumeIcon height={50} /> : <MuteIcon height={50} /> }
            </Button>
          </div>
        </div>
      )}

      {/* Timeline */}
      <div className={classes.AUDIO_MOBILE_TIMELINE}
        onMouseEnter={() => {
          if (isTouchScreen) return
          setTimelineHover(true)
        }}
        onMouseLeave={() => {
          if (isTouchScreen) return
          setTimelineHover(false)
        }}
        onMouseMove={e => {
          if (isTouchScreen) return
          e = e.nativeEvent
          const progress = e.offsetX / window.innerWidth
          const percent = (progress * 100) + '%'
          setScrubberPercent(percent)
        }}
      >

        {/* Overlay */}
        {showLoader && <div className={classes.AUDIO_MOBILE_TIMELINE_LOADER_OVERLAY} />}

        {/* Loader */}
        {showLoader && (
          <div className={classes.AUDIO_MOBILE_TIMELINE_LOADER}>
            <Loader color={theme.vars.ACCENT_COLOR_INVERSE} />
          </div>
        )}

        {/* Scrubber */}
        {!loading && !buffering && timelineHover && (
          <div className={classes.AUDIO_MOBILE_TIMELINE_SCRUBBER} style={{ left: scrubberPercent }} />
        )}

        {/* Background */}
        <div className={classes.AUDIO_MOBILE_TIMELINE_BG} onClick={e => {
          e = e.nativeEvent
          const progress = e.offsetX / window.innerWidth
          cache.get('player').seek(progress)
        }} />

        {/* Progress Bar */}
        <div className={classes.AUDIO_MOBILE_TIMELINE_PROGRESS} style={{ width: (100 * progress) + '%' }} />

        {/* Current Time */}
        <div className={[
          classes.AUDIO_MOBILE_TIMELINE_TIME,
          classes.AUDIO_MOBILE_TIMELINE_CURRENT_TIME,
        ].join(' ')}>
          {
            error ? error
              : loading ? 'Loading...'
              : seeking ? 'Seeking...'
              : buffering ? 'Buffering...'
              : formatTime(currentTime)
          }
        </div>

        {/* Remaining Time */}
        <div className={[
          classes.AUDIO_MOBILE_TIMELINE_TIME,
          classes.AUDIO_MOBILE_TIMELINE_TOTAL_TIME,
        ].join(' ')}>
          {formatTime(totalTime - currentTime)}
        </div>
      </div>

    </ShellMobile>
  )
}

AudioMobile.defaultProps = {
  audio: null
}

export default AudioMobile
