import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import LogoIcon from '../../server/assets/svg/logo-icon.svg'

const useStyles = makeStyles(theme => ({
  IMAGE_ROOT: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    border: `2px solid ${theme.vars.ACCENT_COLOR}`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center'
  },
  IMAGE_PLACEHOLDER: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    background: theme.vars.ACCENT_COLOR_INVERSE,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '& svg path': {
      fill: theme.vars.ACCENT_COLOR
    }
  }
}))

const Image = ({ src }) => {
  const classes = useStyles()

  if (!src) {
    return (
      <div className={classes.IMAGE_ROOT}>
        <div className={classes.IMAGE_PLACEHOLDER}>
          <LogoIcon height="60%" width="60" />
        </div>
      </div>
    )
  }

  return (
    <div
      className={classes.IMAGE_ROOT}
      style={{backgroundImage: `url(${src})`}}
    />
  )
}

Image.defaultProps = {
  src: null,
}

export default Image
