import React from 'react'
import NextApp from 'next/app'
import Head from 'next/head'
import Router from 'next/router'
import _ from 'lodash'
import { makeStyles } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import cache from '../../helpers/cache'
import events from '../../helpers/events'
import actions from '../../helpers/actions'
import Player from './Player'
import themes from '../themes'

const { UPDATE_APP } = actions

// Track url changes
Router.events.on('routeChangeStart', url => {
  const pagesVisited = cache.get('pages-visited') || 0
  cache.set('pages-visited', pagesVisited + 1)
})

class App extends NextApp {
  constructor() {
    super()
    this.actions = actions
    this.themes = themes
    this.defaultTheme = 'sexy-aqua'
    this.state = { theme: this.defaultTheme }
    cache.set('app', this)
  }

  componentDidMount() {
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) jssStyles.parentNode.removeChild(jssStyles)
  }

  componentDidUpdate() {
    events.dispatch(UPDATE_APP, this.state)
  }

  getTheme = () => themes[this.state.theme]

  setTheme = theme => {
    const nextTheme = themes[theme] || themes[this.defaultTheme]
    const nextState = { theme: nextTheme.name }
    this.setState(nextState)
  }

  render() {
    const { Component, pageProps, router } = this.props
    const theme = themes[this.state.theme] || themes[this.defaultTheme]

    return (
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...pageProps} />
        <Player />
      </ThemeProvider>
    )
  }
}

export default App
