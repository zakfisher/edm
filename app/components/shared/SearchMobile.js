import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import _ from 'lodash'
import ShellMobile from './ShellMobile'
import Router from 'next/router'
import cache from '../../helpers/cache'
import events from '../../helpers/events'
import actions from '../../helpers/actions'
import CloseIcon from '../../server/assets/svg/close-icon.svg'
import PlayIcon from '../../server/assets/svg/play-icon.svg'
import PauseIcon from '../../server/assets/svg/pause-icon.svg'
import CaretRightIcon from '../../server/assets/svg/caret-right-icon.svg'
import CaretLeftIcon from '../../server/assets/svg/caret-left-icon.svg'
import CaretDoubleRightIcon from '../../server/assets/svg/caret-double-right-icon.svg'
import CaretDoubleLeftIcon from '../../server/assets/svg/caret-double-left-icon.svg'
import Loader from './Loader'
import Image from './Image'
import uid from '../../helpers/uid'
import { searchAudio, searchPodcasts, searchEvents } from '../../helpers/api'
import { getImageSrc, getParentTitle, getPageUrl } from '../../helpers/audio'
import * as podcastHelper from '../../helpers/podcast'
const { UPDATE_PLAYER } = actions

const useStyles = makeStyles(theme => ({
  SEARCH_MOBILE_ROOT: {},
  SEARCH_MOBILE_CONTENT: {
    paddingTop: theme.vars.SEARCH_MOBILE_INPUT_HEIGHT
  },
  SEARCH_MOBILE_INPUT: {
    background: theme.vars.ACCENT_COLOR,
    height: theme.vars.SEARCH_MOBILE_INPUT_HEIGHT,
    width: '100%',
    position: 'fixed',
    left: 0,
    right: 0,
    top: theme.vars.PAGE_BORDER_HEIGHT,
    zIndex: 1,
    '& input': {
      outline: 'none',
      border: 'none',
      background: 'transparent',
      position: 'absolute',
      top: 0,
      bottom: theme.vars.PAGE_BORDER_HEIGHT,
      left: 0,
      right: 0,
      color: theme.vars.ACCENT_COLOR_INVERSE,
      fontSize: 24,
      fontWeight: 'bold',
      width: '100%',
      padding: '0 50px 0 20px',
      '&::placeholder': {
        color: theme.vars.ACCENT_COLOR_GRAY
      }
    },
    '& svg path': {
      fill: theme.vars.ACCENT_COLOR_GRAY
    }
  },
  SEARCH_MOBILE_INPUT_CLOSE: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 20,
    margin: 'auto',
    height: 22,
    width: 22,
    cursor: 'pointer',
    transform: `translateY(-${theme.vars.PAGE_BORDER_HEIGHT / 2}px)`
  },
  SEARCH_MOBILE_TABS: {
    borderBottom: `2px solid ${theme.vars.ACCENT_COLOR}`,
    position: 'fixed',
    left: 0,
    right: 0,
    top: theme.vars.PAGE_BORDER_HEIGHT + theme.vars.SEARCH_MOBILE_INPUT_HEIGHT,
    zIndex: 1,
    height: theme.vars.SEARCH_MOBILE_TABS_HEIGHT,
    width: '100%',
    background: theme.vars.SEARCH_MOBILE_TABS_BG_COLOR,
    display: 'flex',
    '& > div': {
      width: '33.33%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      cursor: 'pointer',
      transition: 'background 300ms ease',
      background: theme.vars.BUTTON_HOVER_BG_COLOR_INVERSE,
      '& *': {
        display: 'flex',
        margin: 0,
        padding: 0
      },
      '& p': {
        fontSize: 14,
        fontWeight: 'bold',
        color: theme.vars.ACCENT_COLOR_INVERSE,
        textTransform: 'uppercase'
      },
      '& span': {
        fontSize: 12,
        fontWeight: 'bold',
        color: theme.vars.ACCENT_COLOR_INVERSE
      },
      '&.active': {
        background: theme.vars.BUTTON_HOVER_BG_COLOR,
        '& p': {
          color: theme.vars.ACCENT_COLOR
        },
        '& span': {
          color: theme.vars.ACCENT_COLOR
        }
      },
      '&:not(.active):hover': theme.mixins.hover({
        background: theme.vars.ACCENT_COLOR,
        // background: theme.vars.ACTIVE_TAB_BG_COLOR,
      }),
      '&:nth-child(2)': {
        borderLeft: `2px solid ${theme.vars.ACCENT_COLOR}`,
        borderRight: `2px solid ${theme.vars.ACCENT_COLOR}`,
      },
    }
  },
  SEARCH_MOBILE_VIEWS: {
    paddingTop: theme.vars.SEARCH_MOBILE_TABS_HEIGHT,
    width: '100%',
  },
  SEARCH_MOBILE_LIST: {
    '&.paginated': {
      paddingBottom: theme.vars.SEARCH_MOBILE_PAGINATION_HEIGHT
    }
  },
  SEARCH_MOBILE_LIST_ITEM: {
    background: theme.vars.BUTTON_HOVER_BG_COLOR,
    height: theme.vars.SEARCH_MOBILE_LIST_ITEM_HEIGHT,
    transition: 'background 300ms ease',
    cursor: 'pointer',
    display: 'flex',
    borderBottom: `2px solid ${theme.vars.ACCENT_COLOR}`,
    '&.paginated:last-child': {
      borderBottom: 'none'
    },
    '& *': {
      margin: 0,
      padding: 0
    },
    '& > div': {
      height: theme.vars.SEARCH_MOBILE_LIST_ITEM_HEIGHT
    },
    '&:hover': theme.mixins.hover({
      background: theme.vars.ACCENT_COLOR_INVERSE
    }),
  },
  SEARCH_MOBILE_LIST_ITEM_IMAGE: {
    // border: '1px solid red',
    minWidth: theme.vars.SEARCH_MOBILE_LIST_ITEM_HEIGHT,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '& > div': {
      height: '75%',
      width: '75%',
    }
  },
  SEARCH_MOBILE_LIST_ITEM_TEXT: {
    // border: '1px solid red',
    width: `calc(100% - ${theme.vars.SEARCH_MOBILE_LIST_ITEM_HEIGHT + 50}px)`,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '0 5px'
  },
  SEARCH_MOBILE_LIST_ITEM_TEXT_TITLE: {
    fontSize: 16,
    fontWeight: 'bold',
    color: theme.vars.ACCENT_COLOR,
    ...theme.mixins.ellipsis()
  },
  SEARCH_MOBILE_LIST_ITEM_TEXT_SUBTITLE: {
    fontSize: 14,
    fontWeight: 'bold',
    color: theme.vars.ACCENT_COLOR_DARK_GRAY,
    ...theme.mixins.ellipsis()
  },
  SEARCH_MOBILE_LIST_ITEM_ICON: {
    // border: '1px solid red',
    width: 50,
    '& svg': {
      position: 'absolute',
      top: 0,
      bottom: 0,
      right: 20
    }
  },
  SEARCH_MOBILE_PAGINATION: {
    borderTop: `2px solid ${theme.vars.ACCENT_COLOR}`,
    background: theme.vars.PAGINATION_BG_COLOR,
    bottom: theme.vars.PAGE_BORDER_HEIGHT + theme.vars.PLAYER_MOBILE_HEIGHT,
    position: 'fixed',
    height: theme.vars.SEARCH_MOBILE_PAGINATION_HEIGHT,
    width: '100%',
    display: 'flex',
    '& > div': {
      // border: '1px solid red',
      height: '100%',
      width: '20%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& svg': {
        // border: '1px solid red',
        cursor: 'pointer'
      }
    }
  },
  SEARCH_MOBILE_PAGINATION_FIRST: {},
  SEARCH_MOBILE_PAGINATION_PREV: {},
  SEARCH_MOBILE_PAGINATION_CURRENT: {
    '& input': {
      border: 'none',
      borderRadius: theme.vars.SMALL_INPUT_HEIGHT / 2,
      height: theme.vars.SMALL_INPUT_HEIGHT,
      width: 70,
      outline: 'none',
      padding: '0 15px',
      fontSize: 14,
      fontWeight: 'bold',
      cursor: 'text',
      color: theme.vars.ACCENT_COLOR_INVERSE,
      textShadow: 'none',
      boxShadow: 'none',
      background: theme.vars.ACCENT_COLOR,
      textAlign: 'center'
      // transition: 'background 300ms ease',
      // '&:hover': theme.mixins.hover({
      //   background: theme.vars.INPUT_HOVER_BG_COLOR
      // })
    }
  },
  SEARCH_MOBILE_PAGINATION_NEXT: {},
  SEARCH_MOBILE_PAGINATION_LAST: {},
  SEARCH_MOBILE_LOADER_SVG: {
    height: 0.5 * theme.vars.PLAYER_MOBILE_HEIGHT,
    width: 0.5 * theme.vars.PLAYER_MOBILE_HEIGHT,
  },
  SEARCH_MOBILE_RESULTS_LOADER: {
    position: 'fixed',
    bottom: theme.vars.PAGE_BORDER_HEIGHT + theme.vars.PLAYER_MOBILE_HEIGHT,
    top: theme.vars.PAGE_BORDER_HEIGHT + theme.vars.SEARCH_MOBILE_INPUT_HEIGHT,
    width: '100%',
    background: theme.vars.ACTIVE_TAB_BG_COLOR,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    '& p': {
      color: theme.vars.ACCENT_COLOR,
      fontSize: 14,
      fontWeight: 'bold',
      textAlign: 'center'
    }
  },
  SEARCH_MOBILE_PAGE_LOADER: {
    top: theme.vars.PAGE_BORDER_HEIGHT + theme.vars.SEARCH_MOBILE_INPUT_HEIGHT + theme.vars.SEARCH_MOBILE_TABS_HEIGHT,
    bottom: theme.vars.PAGE_BORDER_HEIGHT + theme.vars.PLAYER_MOBILE_HEIGHT + theme.vars.SEARCH_MOBILE_PAGINATION_HEIGHT
  },
}))

const close = () => {
  const pagesVisited = cache.get('pages-visited') || 0
  if (pagesVisited) return Router.back()
  Router.replace('/')
}

const getLimit = () => {
  return 25
}

const executeGlobalSearch = async () => {
  const query = cache.get('search-query') || ''
  const offset = 0
  const limit = getLimit()
  const mixes = await searchAudio(query, offset, limit, 'slug,title,feed,event')
  const events = await searchEvents(query, offset, limit, 'slug,title,image.url')
  const podcasts = await searchPodcasts(query, offset, limit, 'slug,title,image.url,lastUpdated')
  return { mixes, events, podcasts }
}

const handleKeyboardEvents = e => {
  // console.log(e.keyCode)
  const keys = {}

  // ESC - close
  keys[27] = () => close()

  const fn = keys[e.keyCode] || null
  if (!fn) return
  e.preventDefault()
  fn()
}

const addKeyboardEvents = () => {
  window.document.addEventListener('keydown', handleKeyboardEvents)
}

const removeKeyboardEvents = () => {
  window.document.removeEventListener('keydown', handleKeyboardEvents)
}

const selectTab = (tab, setActiveTab) => {
  setActiveTab(tab)
  cache.set('search-tab', tab)
  const params = [
    `query=${cache.get('search-query') || ''}`,
    `tab=${tab}`
  ]
  Router.replace(`/search?${params.join('&')}`)
}

const formatNumber = num => {
  return num.toString().split('').reverse().map((n, i) => {
    return `${n}${(i > 0 && i % 3 === 0) ? ',' : ''}`
  }).reverse().join('')
}

const SearchMobile = () => {
  const classes = useStyles()
  const theme = cache.get('app').getTheme()

  const [activeTab, setActiveTab] = React.useState(cache.get('search-tab') || 'mixes')
  const [inputId, setInputId] = React.useState(null)
  const [searching, setSearching] = React.useState(false)
  const [mixes, setMixes] = React.useState(null)
  const [podcasts, setPodcasts] = React.useState(null)
  const [events, setEvents] = React.useState(null)

  const search = async () => {
    if (searching) return
    setSearching(true)
    const res = await executeGlobalSearch()
    setSearching(false)
    setMixes(res.mixes)
    setPodcasts(res.podcasts)
    setEvents(res.events)
  }

  React.useEffect(() => {
    // Invoke initial search
    search()

    // Focus on input
    const inputId = uid('search-input')
    setInputId(inputId)
    window.requestAnimationFrame(() => {
      window.document.getElementById(inputId).focus()
    })

    // Swap keyboard events w/player
    const player = cache.get('player')
    player.removeKeyboardEvents()
    addKeyboardEvents()

    return () => {
      removeKeyboardEvents()
      player.addKeyboardEvents()
    }
  }, [])

  const Input = (
    <div className={classes.SEARCH_MOBILE_INPUT}>
      <input
        id={inputId}
        type="text"
        placeholder="Search"
        value={cache.get('search-query') || ''}
        onChange={e => {
          const query = e.target.value
          cache.set('search-query', query)
          const params = [
            `query=${query}`,
            `tab=${cache.get('search-tab') || 'mixes'}`
          ]
          Router.replace(`/search?${params.join('&')}`)
        }}
        onKeyDown={e => {
          // ENTER - search
          if (e.keyCode === 13) search()
        }}
      />
      <div className={classes.SEARCH_MOBILE_INPUT_CLOSE} onClick={close}>
        <CloseIcon />
      </div>
    </div>
  )

  const Tabs = (
    <div className={classes.SEARCH_MOBILE_TABS}>
      <div
        className={activeTab === 'mixes' ? 'active' : null}
        onClick={() => selectTab('mixes', setActiveTab)}
      >
        <p>Mixes</p>
        <span>{formatNumber(_.get(mixes, 'total', 0))}</span>
      </div>
      <div
        className={activeTab === 'podcasts' ? 'active' : null}
        onClick={() => selectTab('podcasts', setActiveTab)}
      >
        <p>Podcasts</p>
        <span>{formatNumber(_.get(podcasts, 'total', 0))}</span>
      </div>
      <div
        className={activeTab === 'events' ? 'active' : null}
        onClick={() => selectTab('events', setActiveTab)}
      >
        <p>Events</p>
        <span>{formatNumber(_.get(events, 'total', 0))}</span>
      </div>
    </div>
  )

  const Views = (
    <div className={classes.SEARCH_MOBILE_VIEWS}>
      {mixes && activeTab === 'mixes' && (
        <List
          classes={classes}
          searching={searching}
          mixes={mixes}
        />
      )}
      {podcasts && activeTab === 'podcasts' && (
        <List
          classes={classes}
          searching={searching}
          podcasts={podcasts}
        />
      )}
      {events && activeTab === 'events' && (
        <List
          classes={classes}
          searching={searching}
          events={events}
        />
      )}
    </div>
  )

  return (
    <ShellMobile
      className={classes.SEARCH_MOBILE_ROOT}
      contentClassName={classes.SEARCH_MOBILE_CONTENT}
      header={false}
    >
      {Input}
      {searching ? (
        <div className={classes.SEARCH_MOBILE_RESULTS_LOADER}>
          <div className={classes.SEARCH_MOBILE_LOADER_SVG}>
            <Loader color={theme.vars.ACCENT_COLOR} />
          </div>
          <p>Searching for bangers...</p>
        </div>
      ) : (
        <React.Fragment>
          {Tabs}
          {Views}
        </React.Fragment>
      )}
    </ShellMobile>
  )
}

const List = ({
  classes,
  searching,
  mixes,
  events,
  podcasts
}) => {
  if (searching) return null

  const [loading, setLoading] = React.useState(false)
  const theme = cache.get('app').getTheme()

  const {
    results,
    limit,
    offset,
    total
  } = mixes || events || podcasts

  const isPaginated = total > limit

  return (
    <React.Fragment>
      <div className={[
        classes.SEARCH_MOBILE_LIST,
        isPaginated ? 'paginated' : ''
      ].join(' ')}>
        {loading && (
          <div className={[
            classes.SEARCH_MOBILE_RESULTS_LOADER,
            classes.SEARCH_MOBILE_PAGE_LOADER
          ].join(' ')}>
            <div className={classes.SEARCH_MOBILE_LOADER_SVG}>
              <Loader color={theme.vars.ACCENT_COLOR} />
            </div>
            <p>Wait for it...</p>
          </div>
        )}
        {mixes && !loading && results.map((audio, i) => (
          <ListItem
            key={i}
            classes={classes}
            paginated={isPaginated}
            title={audio.title}
            subtitle={getParentTitle(audio)}
            imageSrc={getImageSrc(audio, '?tr=w-100')}
            Icon={() => <PlayPause audio={audio} />}
            onClick={() => {
              const player = cache.get('player')
              const currentAudioId = _.get(player, 'state.audio._id', null)
              if (currentAudioId && currentAudioId !== audio._id) {
                player.setAudio(audio)
                player.play()
              }
              else if (currentAudioId === audio._id) {
                player[player.state.playing ? 'pause' : 'play']()
              }
            }}
          />
        ))}
        {podcasts && !loading && results.map((podcast, i) => (
          <ListItem
            key={i}
            classes={classes}
            paginated={isPaginated}
            title={podcast.title}
            subtitle={podcastHelper.getLastUpdated(podcast)}
            imageSrc={podcastHelper.getImageSrc(podcast, '?tr=w-100')}
            Icon={() => <CaretRightIcon height="100%" width="30%" />}
            onClick={() => {
              console.log('click podcast', podcast)
            }}
          />
        ))}
        {events && !loading && results.map((event, i) => (
          <ListItem
            key={i}
            classes={classes}
            paginated={isPaginated}
            title={event.title}
            subtitle={null}
            imageSrc={null}
            Icon={() => <CaretRightIcon height="100%" width="30%" />}
            onClick={() => {
              console.log('click event', event)
            }}
          />
        ))}
      </div>
      {isPaginated && (
        <Pagination
          classes={classes}
          offset={offset}
          limit={limit}
          total={total}
        />
      )}
    </React.Fragment>
  )
}

const ListItem = ({
  classes,
  paginated,
  title,
  subtitle,
  imageSrc,
  Icon,
  onClick
}) => {
  return (
    <div className={[
      classes.SEARCH_MOBILE_LIST_ITEM,
      paginated ? 'paginated' : ''
    ].join(' ')} onClick={onClick}>
      <div className={classes.SEARCH_MOBILE_LIST_ITEM_IMAGE}>
        <div>
          <Image src={imageSrc} />
        </div>
      </div>
      <div className={classes.SEARCH_MOBILE_LIST_ITEM_TEXT}>
        {title && <div className={classes.SEARCH_MOBILE_LIST_ITEM_TEXT_TITLE}>{title}</div>}
        {subtitle && <div className={classes.SEARCH_MOBILE_LIST_ITEM_TEXT_SUBTITLE}>{subtitle}</div>}
      </div>
      <div className={classes.SEARCH_MOBILE_LIST_ITEM_ICON}>
        <Icon />
      </div>
    </div>
  )
}

const PlayPause = ({ audio }) => {
  const player = cache.get('player')
  const currentAudioId = _.get(player, 'state.audio._id', null)

  const [isPauseIcon, setIsPauseIcon] = React.useState(currentAudioId === audio._id)

  React.useEffect(() => {
    const playerEventKey = events.on(UPDATE_PLAYER, state => {
      setIsPauseIcon(state.playing && state.audio._id === audio._id)
    })
    return () => events.off(playerEventKey)
  }, [])

  return isPauseIcon
    ? <PauseIcon height="100%" width="60%" />
    : <PlayIcon height="100%" width="60%" />
}

const Pagination = ({
  classes,
  offset,
  limit,
  total
}) => {

  const getCurrentPage = () => {
    return 1
  }

  const [currentPage, setCurrentPage] = React.useState(getCurrentPage())

  return (
    <div className={classes.SEARCH_MOBILE_PAGINATION}>
      <div className={classes.SEARCH_MOBILE_PAGINATION_FIRST}>
        <CaretDoubleLeftIcon height={22} width={40} />
      </div>
      <div className={classes.SEARCH_MOBILE_PAGINATION_PREV}>
        <CaretLeftIcon height={22} width={40} />
      </div>
      <div className={classes.SEARCH_MOBILE_PAGINATION_CURRENT}>
        <input
          type="text"
          value={currentPage}
          onChange={e => {
            setCurrentPage(e.target.value.replace(/[^0-9]/g, ''))
          }}
        />
      </div>
      <div className={classes.SEARCH_MOBILE_PAGINATION_NEXT}>
        <CaretRightIcon height={22} width={40} />
      </div>
      <div className={classes.SEARCH_MOBILE_PAGINATION_LAST}>
        <CaretDoubleRightIcon height={22} width={40} />
      </div>
    </div>
  )
}

export default SearchMobile
