import React from 'react'
import Page from '../shared/Page'
import PodcastList from '../shared/PodcastList'
import { getPodcasts } from '../../helpers/api'
import cache from '../../helpers/cache'
import isClient from '../../helpers/is-client'

const head = {
  title: 'EDM Town - Podcasts'
}

const getXhrPodcasts = async setPodcasts => {
  const podcasts = await getPodcasts()
  setPodcasts(podcasts)
}

const Podcasts = props => {
  const [podcasts, setPodcasts] = React.useState(props.podcasts)

  const loading = !podcasts.length
  if (loading) getXhrPodcasts(setPodcasts)
  else cache.set('podcasts', podcasts)

  return (
    <Page head={head} href="/podcasts">
      {loading && <p>loading...</p>}
      {!loading && <PodcastList podcasts={podcasts} />}
    </Page>
  )
}

Podcasts.getInitialProps = async () => {
  const props = {
    podcasts: []
  }
  if (!isClient) {
    props.podcasts = await getPodcasts()
  }
  else {
    props.podcasts = cache.get('podcasts') || []
  }
  return props
}

export default Podcasts
