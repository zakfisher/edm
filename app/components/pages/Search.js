import React from 'react'
import _ from 'lodash'
import Page from '../shared/Page'
import cache from '../../helpers/cache'
import SearchMobile from '../shared/SearchMobile'
const SearchDesktop = SearchMobile

const Search = ({ query, tab }) => {
  cache.set('search-query', query)
  cache.set('search-tab', tab)

  return (
    <Page
      href="/"
      head={{ title: 'EDM Town' }}
      Mobile={SearchMobile}
      Desktop={SearchDesktop}
    />
  )
}

Search.getInitialProps = context => {
  const props = {}
  props.query = _.get(context, 'query.query', '')
  props.tab = _.get(context, 'query.tab', '')
  return props
}

export default Search
