import React from 'react'
import Page from '../shared/Page'
// import EventList from '../shared/EventList'
import { getEvents } from '../../helpers/api'
import cache from '../../helpers/cache'
import isClient from '../../helpers/is-client'
import Link from 'next/link'

const head = {
  title: 'EDM Town - Events'
}

const getXhrEvents = async setEvents => {
  const events = await getEvents()
  setEvents(events)
}

const Events = props => {
  const [events, setEvents] = React.useState(props.events)

  const loading = !events.length
  if (loading) getXhrEvents(setEvents)
  else cache.set('events', events)

  return (
    <Page head={head} href="/events">
      {/*<EventList events={props.events} />*/}

      {loading ? <p>loading...</p> : events.map(p => (
        <Link key={p._id} href={`/event?slug=${p.slug}`} as={`/event/${p.slug}`}>
          <a style={{float:'left',clear:'left'}}>{p.title}</a>
        </Link>
      ))}

    </Page>
  )
}

Events.getInitialProps = async () => {
  const props = {
    events: []
  }
  if (!isClient) {
    props.events = await getEvents()
  }
  else {
    props.events = cache.get('events') || []
  }
  return props
}

export default Events
