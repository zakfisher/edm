import React from 'react'
import _ from 'lodash'
import Page from '../shared/Page'
import AudioMobile from '../shared/AudioMobile'
const AudioDesktop = AudioMobile
import isClient from '../../helpers/is-client'
import { getPodcastEpisode, getEventLiveSet } from '../../helpers/api'
import cache from '../../helpers/cache'
import events from '../../helpers/events'
import actions from '../../helpers/actions'
const { UPDATE_PLAYER, PLAYER_READY } = actions

const Audio = props => {
  const [audio, setAudio] = React.useState(props.audio)

  // Set initial audio
  React.useEffect(() => {
    const player = cache.get('player')
    if (audio && player && !player.state.audio) {
      player.setAudio(audio)
    }
  }, [audio])

  // Blow away the current audio so we can fetch the next one
  if (audio && audio.slug !== props.audioSlug) {
    setAudio(null)
  }

  // Fetch the next one
  if (!audio) {
    getXhrAudio(
      props.eventSlug,
      props.podcastSlug,
      props.audioSlug,
      setAudio
    )
  }

  return (
    <Page
      href="/"
      head={{
        title: `EDM Town - ${audio ? audio.title : 'Audio'}`,
        keywords: audio && audio.keywords.join(',')
      }}
      Mobile={() => <AudioMobile audio={audio} />}
      Desktop={() => <AudioDesktop audio={audio} />}
    />
  )
}

const getXhrAudio = async (
  eventSlug,
  podcastSlug,
  audioSlug,
  setAudio
) => {
  if (eventSlug) {
    const audio = await getEventLiveSet(eventSlug, audioSlug)
    setAudio(audio)
  }
  if (podcastSlug) {
    const audio = await getPodcastEpisode(podcastSlug, audioSlug)
    setAudio(audio)
  }
}

Audio.getInitialProps = async context => {
  const eventSlug = _.get(context, 'query.eventSlug', null)
  const podcastSlug = _.get(context, 'query.podcastSlug', null)
  const audioSlug = _.get(context, 'query.audioSlug', null)
  return {
    eventSlug,
    podcastSlug,
    audioSlug,
    audio: isClient ? null
      : eventSlug ? await getEventLiveSet(eventSlug, audioSlug)
      : podcastSlug ? await getPodcastEpisode(podcastSlug, audioSlug)
      : null
  }
}

export default Audio
