import React from 'react'
import _ from 'lodash'
import Page from '../shared/Page'
import { getPodcastById, getPodcastAudio } from '../../helpers/api'
import isClient from '../../helpers/is-client'
import Link from 'next/link'

const getXhrPodcast = async (slug, setPodcast, setLiveSets) => {
  const [ podcast, episodes ] = await Promise.all([
    getPodcastById(slug),
    getPodcastAudio(slug)
  ])
  setPodcast(podcast)
  setLiveSets(episodes)
}

const Podcast = props => {
  const [podcast, setPodcast] = React.useState(props.podcast)
  const [episodes, setLiveSets] = React.useState(props.episodes)
  const loading = !podcast
  if (loading) getXhrPodcast(props.slug, setPodcast, setLiveSets)

  const head = {
    title: `EDM Town - ${podcast ? podcast.title : 'Podcast'}`
  }

  return (
    <Page head={head} href="/podcasts">
      {podcast && <h1>{podcast.title}</h1>}
      {loading ? <p>loading...</p> : episodes.map(audio => (
        <Link key={audio._id}
          href={`/audio?podcastSlug=${podcast.slug}&audioSlug=${audio.slug}`}
          as={`/podcast/${podcast.slug}/episode/${audio.slug}`}>
          <a style={{float:'left',clear:'left'}}>{audio.title}</a>
        </Link>
      ))}
    </Page>
  )
}

Podcast.getInitialProps = async context => {
  const slug = _.get(context, 'query.slug', null)
  const props = {
    slug,
    podcast: null,
    episodes: []
  }
  if (!isClient) {
    const [ podcast, episodes ] = await Promise.all([
      getPodcastById(slug),
      getPodcastAudio(slug)
    ])
    props.podcast = podcast
    props.episodes = episodes
  }
  return props
}

export default Podcast
