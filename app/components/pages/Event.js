import React from 'react'
import _ from 'lodash'
import Page from '../shared/Page'
// import EventList from '../shared/EventList'
import { getEventById, getEventAudio } from '../../helpers/api'
// import cache from '../../helpers/cache'
import isClient from '../../helpers/is-client'
import Link from 'next/link'

const getXhrEvent = async (slug, setEvent, setLiveSets) => {
  const [ event, liveSets ] = await Promise.all([
    getEventById(slug),
    getEventAudio(slug)
  ])
  setEvent(event)
  setLiveSets(liveSets)
}

const Event = props => {
  const [event, setEvent] = React.useState(props.event)
  const [liveSets, setLiveSets] = React.useState(props.liveSets)
  const loading = !event
  if (loading) getXhrEvent(props.slug, setEvent, setLiveSets)

  const head = {
    title: `EDM Town - ${event ? event.title : 'Event'}`
  }

  return (
    <Page head={head} href="/events">
      {event && <h1>{event.title}</h1>}
      {loading ? <p>loading...</p> : liveSets.map(audio => (
        <Link key={audio._id}
          href={`/audio?eventSlug=${event.slug}&audioSlug=${audio.slug}`}
          as={`/event/${event.slug}/live-set/${audio.slug}`}>
          <a style={{float:'left',clear:'left'}}>{audio.title}</a>
        </Link>
      ))}
    </Page>
  )
}

Event.getInitialProps = async context => {
  const slug = _.get(context, 'query.slug', null)
  const props = {
    slug,
    event: null,
    liveSets: []
  }
  if (!isClient) {
    const [ event, liveSets ] = await Promise.all([
      getEventById(slug),
      getEventAudio(slug)
    ])
    props.event = event
    props.liveSets = liveSets
  }
  return props
}

export default Event
