import React from 'react'
import Page from '../shared/Page'
import HomeMobile from '../shared/HomeMobile'
const HomeDesktop = HomeMobile

const Home = () => (
  <Page
    href="/"
    head={{ title: 'EDM Town' }}
    Mobile={HomeMobile}
    Desktop={HomeDesktop}
  />
)

export default Home
