import { createMuiTheme } from '@material-ui/core/styles'

const colors = {
  black: [
    '#000',
    '#111',
    '#222',
    '#333',
  ],
  gray: [
    '#eee',
    '#ccc',
    '#aaa',
    '#999',
    '#666',
  ],
  white: [
    '#fff'
  ],
  teal: [
    '#0092b3'
  ]
}

const vars = {
  desktopTimelineHeight: 6,
  headerHeight: 50,
  sidebarWidth: 220,
  shellZIndex: 1000
}

const mixins = {
  ellipsis: () => ({

  })
}

// Create a theme instance.
const theme = createMuiTheme({
  name: 'default',
  colors,
  mixins,
  vars,
})

export default theme
