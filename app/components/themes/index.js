import sexyTheme from './sexy'

const themes = {}

const addTheme = (name, theme, scheme) => {
  themes[name] = sexyTheme(name, scheme)
}

addTheme('sexy-aqua', sexyTheme, {
  color1: 'aqua',
  color2: 'deeppink',
  inverted: false,
  images: 'edc'
})

addTheme('sexy-red', sexyTheme, {
  color1: 'crimson',
  color2: 'black',
  inverted: true,
  images: 'edc'
})

addTheme('sexy-purple', sexyTheme, {
  color1: 'purple',
  color2: 'black',
  inverted: true,
  images: 'edc'
})

addTheme('vitalii', sexyTheme, {
  color1: 'blue',
  color2: 'purple',
  inverted: true,
  images: 'edc'
})

export default themes
