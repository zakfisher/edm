import isTouchScreen from '../../helpers/is-touch-screen'

export const ellipsis = (width = '100%') => ({
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  width
})

export const hover = style => {
  return isTouchScreen ? {} : style
}
