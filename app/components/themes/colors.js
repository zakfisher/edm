/* COLORS */
export const black = [
  '#000',
  '#111',
  '#222',
  '#333',
]
export const gray = [
  '#eee',
  '#ccc',
  '#aaa',
  '#999',
  '#666',
]
export const white = [
  '#fff'
]

export const teal = [
  '#0092b3'
]
export const purple = [
  '#C86DD7',
  '#3023AE'
]

/* GRADIENTS */
export const gradients = {}
gradients.tealPurpleHorizontal = `linear-gradient(to right, ${teal[0]}, ${purple[0]})`
