import { createMuiTheme } from '@material-ui/core/styles'
import _ from 'lodash'
import * as colors from '../colors'
import * as mixins from '../mixins'

const imageGroups = {
  edc: ['/assets/backgrounds/EDC-Las-Vegas-1-1068x712.jpg']
}

const sexyTheme = (name, scheme) => {
  const {
    color1,
    color2,
    inverted,
    images
  } = scheme

  const vars = {}

  // Colors
  vars.COLOR_1 = color1
  vars.COLOR_2 = color2
  vars.ACCENT_COLOR = inverted ? colors.white[0] : colors.black[0]
  vars.ACCENT_COLOR_INVERSE = inverted ? colors.black[0] : colors.white[0]
  vars.ACCENT_COLOR_GRAY = inverted ? '#555' : '#999'
  vars.ACCENT_COLOR_DARK_GRAY = inverted ? '#999' : '#555'
  vars.BUTTON_HOVER_BG_COLOR = inverted ? `rgba(0, 0, 0, 0.75)` : `rgba(255, 255, 255, 0.75)`
  vars.BUTTON_HOVER_BG_COLOR_INVERSE = inverted ? `rgba(255, 255, 255, 0.75)` : `rgba(0, 0, 0, 0.75)`
  vars.INPUT_HOVER_BG_COLOR = vars.BUTTON_HOVER_BG_COLOR
  vars.PAGINATION_BG_COLOR = vars.BUTTON_HOVER_BG_COLOR
  vars.SEARCH_MOBILE_TABS_BG_COLOR = 'transparent'

  // Gradients
  vars.GRADIENT_V = `linear-gradient(to bottom, ${color1} 0%, ${color2} 100%)`
  vars.GRADIENT_H = `linear-gradient(to right, ${color1} 0%, ${color2} 100%)`

  // Heights
  vars.BUTTON_HEIGHT = 49
  vars.INPUT_HEIGHT = 38
  vars.SMALL_INPUT_HEIGHT = 31
  vars.PAGE_BORDER_HEIGHT = 6
  vars.HEADER_MOBILE_HEIGHT = 80
  vars.PLAYER_MOBILE_HEIGHT = 74
  vars.SEARCH_MOBILE_INPUT_HEIGHT = 84
  vars.SEARCH_MOBILE_TABS_HEIGHT = 51
  vars.SEARCH_MOBILE_PAGINATION_HEIGHT = vars.SEARCH_MOBILE_TABS_HEIGHT
  vars.SEARCH_MOBILE_LIST_ITEM_HEIGHT = vars.PLAYER_MOBILE_HEIGHT

  // Widths
  vars.AUDIO_MOBILE_IMAGE_WIDTH = 286

  // Images
  vars.BG_IMAGE_1 = _.get(imageGroups, `[${images}][0]`, null)

  return createMuiTheme({
    name,
    colors,
    mixins,
    vars
  })
}

export default sexyTheme
