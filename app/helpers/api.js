import axios from 'axios'
import _ from 'lodash'
import isDev from './is-dev'
import cache from './cache'

const PROD_URL = 'https://edm.town/api'
const LOCAL_URL = 'http://localhost:3000/api'

export const api = axios.create({
  baseURL: isDev ? LOCAL_URL : PROD_URL
})

/* AUDIO */

export const getAudioById = async (id) => {
  try {
    const res = await api.get(`/v1/audio/${id}`)
    const audio = _.get(res, 'data.data', null)
    if (!audio) throw new Error('Error fetching audio')
    return audio
  }
  catch(e) {
    console.error(e)
    return null
  }
}

export const getAudioUrl = async (id) => {
  try {
    const res = await api.get(`/v1/audio/${id}`)
    const url = _.get(res, 'data.data.audio', null)
    if (!url) throw new Error('Error fetching url')
    return url
  }
  catch(e) {
    console.error(e)
    return null
  }
}

export const getRandomAudio = async () => {
  try {
    const res = await api.get('/v1/audio/random')
    const audio = _.get(res, 'data.data', null)
    if (!audio) throw new Error('Error fetching audio')
    return audio
  }
  catch(e) {
    console.error(e)
    return null
  }
}

export const searchAudio = async (query = '', offset = 0, limit = 10, fields = 'title') => {
  const params = [
    `query=${query}`,
    `offset=${offset}`,
    `limit=${limit}`,
    `fields=${fields}`,
  ].join('&')
  try {
    const res = await api.get(`/v1/audio/search?${params}`)
    const data = _.get(res, 'data.data', null)
    if (!data) throw new Error('Error fetching audio')
    return data
  }
  catch(e) {
    console.error(e)
    return null
  }
}

/* EVENTS */

export const getEvents = async () => {
  try {
    const res = await api.get('/v1/events')
    const events = _.get(res, 'data.data', null)
    if (!events) throw new Error('Error fetching events')
    return events
  }
  catch(e) {
    console.error(e)
    return []
  }
}

export const getEventById = async (id) => {
  try {
    const res = await api.get(`/v1/events/${id}`)
    const event = _.get(res, 'data.data', null)
    if (!event) throw new Error('Error fetching event')
    return event
  }
  catch(e) {
    console.error(e)
    return null
  }
}

export const getEventAudio = async (id) => {
  try {
    const res = await api.get(`/v1/events/${id}/audio`)
    const audio = _.get(res, 'data.data', null)
    if (!audio) throw new Error('Error fetching audio')
    return audio
  }
  catch(e) {
    console.error(e)
    return []
  }
}

export const getEventLiveSet = async (eventSlug, audioSlug) => {
  try {
    const res = await api.get(`/v1/events/${eventSlug}/audio/${audioSlug}`)
    const audio = _.get(res, 'data.data', null)
    if (!audio) throw new Error('Error fetching audio')
    return audio
  }
  catch(e) {
    console.error(e)
    return null
  }
}

export const searchEvents = async (query = '', offset = 0, limit = 10, fields = 'title') => {
  const params = [
    `query=${query}`,
    `offset=${offset}`,
    `limit=${limit}`,
    `fields=${fields}`,
  ].join('&')
  try {
    const res = await api.get(`/v1/events/search?${params}`)
    const data = _.get(res, 'data.data', null)
    if (!data) throw new Error('Error fetching events')
    return data
  }
  catch(e) {
    console.error(e)
    return null
  }
}

/* PODCASTS */

export const getPodcasts = async () => {
  try {
    const res = await api.get('/v1/podcasts')
    const podcasts = _.get(res, 'data.data', null)
    if (!podcasts) throw new Error('Error fetching podcasts')
    return podcasts
  }
  catch(e) {
    console.error(e)
    return []
  }
}

export const getPodcastById = async (id) => {
  try {
    const res = await api.get(`/v1/podcasts/${id}`)
    const podcast = _.get(res, 'data.data', null)
    if (!podcast) throw new Error('Error fetching podcast')
    return podcast
  }
  catch(e) {
    console.error(e)
    return null
  }
}

export const getPodcastAudio = async (id) => {
  try {
    const res = await api.get(`/v1/podcasts/${id}/audio`)
    const audio = _.get(res, 'data.data', null)
    if (!audio) throw new Error('Error fetching audio')
    return audio
  }
  catch(e) {
    console.error(e)
    return []
  }
}

export const getPodcastEpisode = async (podcastSlug, audioSlug) => {
  try {
    const res = await api.get(`/v1/podcasts/${podcastSlug}/audio/${audioSlug}`)
    const audio = _.get(res, 'data.data', null)
    if (!audio) throw new Error('Error fetching audio')
    return audio
  }
  catch(e) {
    console.error(e)
    return null
  }
}

export const searchPodcasts = async (query = '', offset = 0, limit = 10, fields = 'title') => {
  const params = [
    `query=${query}`,
    `offset=${offset}`,
    `limit=${limit}`,
    `fields=${fields}`,
  ].join('&')
  try {
    const res = await api.get(`/v1/podcasts/search?${params}`)
    const data = _.get(res, 'data.data', null)
    if (!data) throw new Error('Error fetching podcasts')
    return data
  }
  catch(e) {
    console.error(e)
    return null
  }
}
