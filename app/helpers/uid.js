const number = parseInt(Math.random() * 1000000)
export default (prefix = 'uid') => `${prefix}-${number}`
