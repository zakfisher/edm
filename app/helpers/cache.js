import isDev from './is-dev'

const api = {}
const cache = {}

api.set = (key, value) => cache[key] = value
api.get = key => cache[key] || null
api.getCache = () => cache

if (isDev && typeof window !== 'undefined') {
  window.APP.cache = api
}

export default api
