import _ from 'lodash'
import moment from 'moment'

export const getImageSrc = (podcast, suffix = '') => {
  let src = _.get(podcast, 'image.url', null)
  if (src) src += suffix
  return src
}

export const getPageUrl = podcast => {
  if (!podcast) return null

  const href = `/podcast?slug=${podcast.slug}`
  const as = `/podcasts/${podcast.slug}`

  return { href, as }
}

export const getLastUpdated = podcast => {
  if (!podcast || !podcast.lastUpdated) {
    return null
  }
  return moment(podcast.lastUpdated).fromNow()
}
