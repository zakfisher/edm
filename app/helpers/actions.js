const actions = [
  'UPDATE_APP',
  'UPDATE_PLAYER',
  'PLAYER_READY'
]

const actionMap = {}
actions.map(a => actionMap[a] = a)

export default actionMap
