import _ from 'lodash'
import moment from 'moment'

export const formatTime = seconds => {
  seconds = seconds || 0
  const format = seconds >= 3600 ? 'H:mm:ss' : 'mm:ss'
  return moment().startOf('day').seconds(seconds).format(format)
}

export const getImageSrc = (audio, suffix = '') => {
  let src = _.get(audio, 'feed.image.url', null)
    || _.get(audio, 'event.image.url', null)
  if (src) src += suffix
  return src
}

export const getParentTitle = audio => {
  return _.get(audio, 'event.title', null)
    || _.get(audio, 'feed.title', null)
}

export const getProgressPercent = progress => {
  progress = Math.floor(100 * progress)
  if (progress > 100) progress = 100
  return progress + '%'
}

export const getPageUrl = audio => {
  if (!audio) return null

  let href = ''
  let as = ''

  const audioSlug = _.get(audio, 'slug', null)
  const eventSlug = _.get(audio, 'event.slug', null)
  const podcastSlug = _.get(audio, 'feed.slug', null)

  // Event Live Set
  if (eventSlug) {
    href = `/audio?eventSlug=${eventSlug}&audioSlug=${audioSlug}`
    as = `/event/${eventSlug}/live-set/${audioSlug}`
  }

  // Podcast Episode
  else if (podcastSlug) {
    href = `/audio?podcastSlug=${podcastSlug}&audioSlug=${audioSlug}`
    as = `/podcast/${podcastSlug}/episode/${audioSlug}`
  }

  return { href, as }
}

export const getParentPageUrl = audio => {
  if (!audio) return null

  let href = ''
  let as = ''

  const eventSlug = _.get(audio, 'event.slug', null)
  const podcastSlug = _.get(audio, 'feed.slug', null)

  // Event Live Set
  if (eventSlug) {
    href = `/event?slug=${eventSlug}`
    as = `/event/${eventSlug}`
  }

  // Podcast Episode
  else if (podcastSlug) {
    href = `/podcast?slug=${podcastSlug}`
    as = `/podcast/${podcastSlug}`
  }

  return { href, as }
}
