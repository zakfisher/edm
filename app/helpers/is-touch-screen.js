import isClient from './is-client'
export default isClient && (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0))
