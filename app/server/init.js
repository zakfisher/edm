const express = require('express')
const next = require('next')
const middleware = require('./middleware')
const routes = require('./routes')

const dev = process.env.NODE_ENV === 'dev'
const app = next({ dev })
const server = express()
server.set('app', app)
middleware(server)
routes(server)

module.exports = cb => app.prepare().then(() => server)
