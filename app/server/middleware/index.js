const fs = require('fs')
const cors = require('cors')
const express = require('express')
const favicon = require('serve-favicon')

const ASSETS_DIR = __dirname.replace('middleware', 'assets')
const FAVICON = ASSETS_DIR + '/favicon.png'

module.exports = server => {
  server.use(cors())
  server.use(express.urlencoded({ extended: true }))
  server.use(express.json())
  if (fs.existsSync(FAVICON)) {
    server.use(favicon(FAVICON))
  }
  server.use('/assets', express.static(ASSETS_DIR))
}
