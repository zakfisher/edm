const init = require('./init')
module.exports = (req, res) => init().then(server => server(req, res))
