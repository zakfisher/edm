const fs = require('fs')
const ONE_DAY = 24 * 60 * 60

const routes = server => {
  const app = server.get('app')
  const handle = app.getRequestHandler()

  server.use('/api', require('../api'))

  // server.get('/artist/:slug', (req, res) => {
  //   res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
  //   return app.render(req, res, '/artist', { slug: req.params.slug })
  // })

  server.get('/event/:slug', (req, res) => {
    res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
    return app.render(req, res, '/event', { slug: req.params.slug })
  })

  server.get('/event/:eventSlug/live-set/:audioSlug', (req, res) => {
    res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
    return app.render(req, res, '/audio', {
      eventSlug: req.params.eventSlug,
      audioSlug: req.params.audioSlug,
    })
  })

  server.get('/podcast/:slug', (req, res) => {
    res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
    return app.render(req, res, '/podcast', { slug: req.params.slug })
  })

  server.get('/podcast/:podcastSlug/episode/:audioSlug', (req, res) => {
    res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
    return app.render(req, res, '/audio', {
      podcastSlug: req.params.podcastSlug,
      audioSlug: req.params.audioSlug,
    })
  })

  server.get('*', (req, res) => {
    res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
    return handle(req, res)
  })
}

module.exports = routes
