const getService = require('./get-service')
const convert = getService('utils/convert')

exports.ONE_YEAR = convert(365, 'day', 'sec')
exports.ONE_MONTH = convert(30, 'day', 'sec')
exports.ONE_WEEK = convert(7, 'day', 'sec')
exports.ONE_DAY = convert(1, 'day', 'sec')
exports.ONE_HOUR = convert(1, 'hr', 'sec')
exports.ONE_MINUTE = convert(1, 'min', 'sec')
