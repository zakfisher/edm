module.exports = (res, message, statusCode = 500) => {
  res.status(500).json({
    code: statusCode,
    status: 'error',
    message
  })
}
