const getService = require('./get-service')
const isDev = require('./is-dev')
const connectDB = getService('mongodb/connect-db')
const local = getService('mongodb/credentials/local.json')
const prod = getService('mongodb/credentials/prod.json')

let client = null
module.exports = req => {
  if (client) return client
  const MONGO_URL = isDev(req) ? local.MONGO_URL : prod.MONGO_URL
  client = connectDB('v1', MONGO_URL)
  return client
}
