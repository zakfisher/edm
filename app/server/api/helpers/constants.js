exports.IN_APP_DIR = __dirname.includes('app/server')

exports.IN_FUNCTIONS_DIR = !exports.IN_APP_DIR

exports.SERVICES_DIR = exports.IN_APP_DIR
  ? __dirname.split('/app')[0] + '/scripts/services'
  : __dirname.split('/server')[0] + '/services'

exports.PACKAGE = exports.IN_APP_DIR
  ? __dirname.split('/app')[0] + '/package.json'
  : __dirname.split('/server')[0] + '/package.json'
