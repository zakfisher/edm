const { SERVICES_DIR } = require('./constants')
const getService = service => require(`${SERVICES_DIR}/${service}`)
module.exports = getService
