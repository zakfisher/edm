const getService = require('./get-service')
const getModel = getService('mongodb/get-model')

module.exports = model => getModel(model)
