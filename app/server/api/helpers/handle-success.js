module.exports = (res, data, statusCode = 200) => {
  res.status(200).json({
    code: statusCode,
    status: 'success',
    data
  })
}
