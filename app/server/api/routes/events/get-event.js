const _ = require('lodash')
const connectDB = require('../../helpers/connect-db')
const getModel = require('../../helpers/get-model')
const error = require('../../helpers/handle-error')
const success = require('../../helpers/handle-success')
const { ONE_DAY } = require('../../helpers/time')
const isValidId = require('../../helpers/is-valid-id')

const getEvent = (req, res) => {
  const { slug } = req.params

  if (!slug || !slug.length) {
    return error(res, 'Invalid slug')
  }

  connectDB(req)
    .then(() => {
      const Event = getModel('Event')
      const query = { $or: [{ slug }] }
      if (isValidId(slug)) {
        query.$or.push({ _id: slug })
      }
      return Event.findOne(query)
    })
    .then(event => {
      if (!event) {
        throw new Error('Event not found')
      }
      res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
      success(res, event)
    })
    .catch(err => {
      error(res, _.get(err, 'message', 'Unable to fetch event'))
    })
}

module.exports = getEvent
