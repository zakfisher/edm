const _ = require('lodash')
const connectDB = require('../../helpers/connect-db')
const getModel = require('../../helpers/get-model')
const error = require('../../helpers/handle-error')
const success = require('../../helpers/handle-success')
const { ONE_DAY } = require('../../helpers/time')
const isValidId = require('../../helpers/is-valid-id')

const getEventAudioById = (req, res) => {
  const { slug, audioSlug } = req.params

  if (!slug || !slug.length) {
    return error(res, 'Invalid slug')
  }
  if (!audioSlug || !audioSlug.length) {
    return error(res, 'Invalid audio slug')
  }

  connectDB(req)
    .then(() => {
      const Event = getModel('Event')
      const query = { $or: [{ slug }] }
      if (isValidId(slug)) {
        query.$or.push({ _id: slug })
      }
      return Event.findOne(query)
    })
    .then(event => {
      if (!event) throw new Error('Event not found')

      const Audio = getModel('Audio')
      const query = {
        $or: [{ event: event._id, slug: audioSlug }]
      }
      if (isValidId(audioSlug)) {
        query.$or.push({ event: event._id, _id: audioSlug })
      }

      return Audio.findOne(query)
        .populate('event', 'slug title image.url')
    })
    .then(audio => {
      if (!audio) {
        throw new Error('Audio not found')
      }
      res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
      success(res, audio)
    })
    .catch(err => {
      error(res, _.get(err, 'message', 'Unable to fetch audio'))
    })
}

module.exports = getEventAudioById
