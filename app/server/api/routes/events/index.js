const router = require('express').Router()

router.get('/search', require('./search-events'))
router.get('/', require('./get-events'))
router.get('/:slug', require('./get-event'))
router.get('/:slug/audio', require('./get-event-audio'))
router.get('/:slug/audio/:audioSlug', require('./get-event-audio-by-id'))

module.exports = router
