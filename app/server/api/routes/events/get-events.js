const _ = require('lodash')
const connectDB = require('../../helpers/connect-db')
const getModel = require('../../helpers/get-model')
const error = require('../../helpers/handle-error')
const success = require('../../helpers/handle-success')
const { ONE_DAY } = require('../../helpers/time')

const getEvents = (req, res) => {
  connectDB(req)
    .then(() => {
      const Event = getModel('Event')
      return Event.find({}, 'slug title').sort({ slug: 1 })
    })
    .then(events => {
      const Audio = getModel('Audio')
      return Promise.all(
        events.map(event => {
          return Audio.countDocuments({ event: event._id })
            .then(total => {
              return {
                ...event.toObject(),
                audioCount: total
              }
            })
        })
      )
    })
    .then(events => {
      res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
      success(res, events)
    })
    .catch(err => {
      error(res, _.get(err, 'message', 'Unable to fetch events'))
    })
}

module.exports = getEvents
