const router = require('express').Router()

router.get('/search', require('./search-podcasts'))
router.get('/', require('./get-podcasts'))
router.get('/:slug', require('./get-podcast'))
router.get('/:slug/audio', require('./get-podcast-audio'))
router.get('/:slug/audio/:audioSlug', require('./get-podcast-audio-by-id'))

module.exports = router
