const _ = require('lodash')
const connectDB = require('../../helpers/connect-db')
const getModel = require('../../helpers/get-model')
const error = require('../../helpers/handle-error')
const success = require('../../helpers/handle-success')
const { ONE_DAY } = require('../../helpers/time')

const getFeeds = (req, res) => {
  connectDB(req)
    .then(() => {
      const Feed = getModel('Feed')
      return Feed.find({
        __t: 'PODCAST'
      }, [
        'slug',
        'title',
        'lastUpdated',
        'image.thumbnail',
        'image.url',
        'rss.itunes:author'
      ].join(' '))
      .sort({ title: 1 })
    })
    .then(feeds => {
      const Audio = getModel('Audio')
      return Promise.all(
        feeds.map(feed => {
          return Audio.countDocuments({ feed: feed._id })
            .then(total => {
              return {
                ...feed.toObject(),
                audioCount: total
              }
            })
        })
      )
    })
    .then(feeds => {
      res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
      success(res, feeds)
    })
    .catch(err => {
      error(res, _.get(err, 'message', 'Unable to fetch feeds'))
    })
}

module.exports = getFeeds
