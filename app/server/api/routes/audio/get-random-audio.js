const _ = require('lodash')
const connectDB = require('../../helpers/connect-db')
const getModel = require('../../helpers/get-model')
const getService = require('../../helpers/get-service')
const error = require('../../helpers/handle-error')
const success = require('../../helpers/handle-success')
const randomNumber = getService('utils/random-number')

const getRandomAudio = (req, res) => {
  connectDB(req)
    .then(() => {
      const Audio = getModel('Audio')
      return Audio.countDocuments()
    })
    .then(total => {
      const Audio = getModel('Audio')
      return Audio.findOne()
        .skip(randomNumber(0, total - 1))
        .populate('feed', 'slug title image.url')
        .populate('event', 'slug title image.url')
    })
    .then(audio => {
      if (!audio) throw new Error('Audio not found')
      success(res, audio)
    })
    .catch(err => {
      error(res, _.get(err, 'message', 'Unable to fetch audio'))
    })
}

module.exports = getRandomAudio
