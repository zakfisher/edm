const router = require('express').Router()

router.get('/random', require('./get-random-audio'))
router.get('/search', require('./search-audio'))
router.get('/:id', require('./get-audio'))

module.exports = router
