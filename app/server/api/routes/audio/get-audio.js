const _ = require('lodash')
const connectDB = require('../../helpers/connect-db')
const getModel = require('../../helpers/get-model')
const getService = require('../../helpers/get-service')
const error = require('../../helpers/handle-error')
const success = require('../../helpers/handle-success')
const b2 = getService('backblaze/http')
const { ONE_DAY } = require('../../helpers/time')
const isValidId = require('../../helpers/is-valid-id')

const getAudio = (req, res) => {
  const { id } = req.params

  if (!id || !id.length) {
    return error(res, 'Invalid id')
  }

  connectDB(req)
    .then(() => {
      const Audio = getModel('Audio')
      return Audio.findOne({ _id: id })
        .populate('feed', 'slug')
    })
    .then(audio => {
      if (!audio) {
        throw new Error('Audio not found')
      }
      return Promise.all([
        audio,
        b2.authorize()
      ])
    })
    .then(([ audio, auth ]) => {
      res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
      success(res, {
        ...audio.toObject(),
        audio: audio.audio + `?Authorization=${auth.authorizationToken}`
      })
    })
    .catch(err => {
      error(res, _.get(err, 'message', 'Unable to fetch audio'))
    })
}

module.exports = getAudio
