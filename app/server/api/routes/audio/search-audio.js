const _ = require('lodash')
const connectDB = require('../../helpers/connect-db')
const getModel = require('../../helpers/get-model')
const error = require('../../helpers/handle-error')
const success = require('../../helpers/handle-success')
const { ONE_DAY } = require('../../helpers/time')

const searchAudio = (req, res) => {
  let query = req.query.query || ''

  let offset = 0
  const queryOffsetNumber = Number(req.query.offset)
  if (!isNaN(queryOffsetNumber) && queryOffsetNumber > -1) {
    offset = queryOffsetNumber
  }

  let limit = 100
  const maxLimit = 500
  const queryLimitNumber = Number(req.query.limit)
  if (!isNaN(queryLimitNumber) && queryLimitNumber > -1 && queryLimitNumber <= maxLimit) {
    limit = queryLimitNumber
  }

  let sort = { title: 1 }
  // if (req.query.sort === 'artist') sort = { artist: 1 }
  // TODO: Sort by (publish) date

  const searchQuery = {
    $or: [
      // { artist: { $regex: query, $options: 'i' } },
      // { artists: { $regex: query, $options: 'i' } },
      { title: { $regex: query, $options: 'i' } },
      { keywords: { $regex: query, $options: 'i' } },
    ]
  }

  const fields = (req.query.fields && req.query.fields.split(',').join(' ')) || 'title'

  const getTotal = () => {
    const Audio = getModel('Audio')
    return Audio.countDocuments(searchQuery)
  }

  const getResults = () => {
    const Audio = getModel('Audio')
    return Audio
      .find(searchQuery, fields)
      .populate('feed', 'slug title image.url')
      .populate('event', 'slug title image.url')
      .skip(offset)
      .limit(limit)
      .sort(sort)
  }

  connectDB(req)
    .then(() => Promise.all([
      getTotal(),
      getResults()
    ]))
    .then(([ total, results ]) => {
      res.set('Cache-Control', `public, max-age=${ONE_DAY}, s-maxage=${ONE_DAY}`)
      success(res, {
        offset,
        limit,
        total,
        results
      })
    })
    .catch(err => {
      error(res, _.get(err, 'message', 'Unable to fetch audio'))
    })
}

module.exports = searchAudio
