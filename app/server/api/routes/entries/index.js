const router = require('express').Router()

router.get('/:id', require('./get-entry'))

module.exports = router
