const _ = require('lodash')
const connectDB = require('../../helpers/connect-db')
const getModel = require('../../helpers/get-model')
const error = require('../../helpers/handle-error')
const success = require('../../helpers/handle-success')
const { ONE_YEAR } = require('../../helpers/time')

const getEntry = (req, res) => {
  const { id } = req.params

  if (!id || !id.length) {
    return error(res, 'Invalid id')
  }

  connectDB(req)
    .then(() => {
      const Entry = getModel('Entry')
      return Entry.findOne({ _id: id })
    })
    .then(entry => {
      if (!entry) {
        throw new Error('Entry not found')
      }
      res.set('Cache-Control', `public, max-age=${ONE_YEAR}, s-maxage=${ONE_YEAR}`)
      success(res, entry)
    })
    .catch(err => {
      error(res, _.get(err, 'message', 'Unable to fetch entry'))
    })
}

module.exports = getEntry
