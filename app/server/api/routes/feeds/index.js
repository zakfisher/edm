const router = require('express').Router()

router.get('/', require('./get-feeds'))
router.get('/:slug', require('./get-feed'))
router.get('/:slug/audio', require('./get-feed-audio'))

module.exports = router
