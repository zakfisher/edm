const express = require('express')
const isDev = require('./helpers/is-dev')
const { PACKAGE } = require('./helpers/constants')

const v1 = express.Router()
v1.get('/', (req, res) => {
  const env = isDev(req) ? 'local' : 'production'
  const version = require(PACKAGE).version
  res.json({ name: 'EDM Town API', version, env })
})
v1.use('/audio', require('./routes/audio'))
v1.use('/entries', require('./routes/entries'))
v1.use('/events', require('./routes/events'))
v1.use('/feeds', require('./routes/feeds'))
v1.use('/podcasts', require('./routes/podcasts'))

module.exports = v1
