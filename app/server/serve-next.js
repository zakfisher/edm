const chalk = require('chalk')
const express = require('express')
const init = require('./init')

init().then(server => {
  console.log()
  console.log(chalk.cyan('NODE_ENV'))
  console.log(process.env.NODE_ENV, '\n')

  const port = process.env.PORT || 3000
  server.listen(port, err => {
    if (err) throw err
    console.log(chalk.green('Web Server'))
    console.log(`http://localhost:${port}`, '\n')
  })
})
