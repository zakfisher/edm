const shell = require('shelljs')

const APP_DIR = __dirname.replace('scripts', 'app')
const API_DIR = __dirname.replace('scripts', 'functions')
const SERVER_DIR = __dirname.replace('scripts', 'app/server')
const COMPONENTS_DIR = __dirname.replace('scripts', 'app/components')

const SVG_SRC = APP_DIR + '/server/assets/svg'
const SVG_DEST = APP_DIR + '/components/icons'

const startServer = 'NODE_ENV=dev node ./server/serve-next.js'

const watch = [
  `cd ${APP_DIR} &&`,
  `nodemon --exec "${startServer}"`,
  `--watch ${API_DIR}`,
  `--watch ${SERVER_DIR}`,
  `--watch ${COMPONENTS_DIR}/shared/Document.js`,
  '--color'
].join(' ')

shell.exec(watch)
