const fs = require('fs')
const shell = require('shelljs')
const chalk = require('chalk')

const COMMANDS_DIR = __dirname + '/commands'

const args = process.argv.slice(2)
const command = args.shift()
const COMMAND_PATH = command ? `${COMMANDS_DIR}/${command}` : null

if (!fs.existsSync(COMMAND_PATH)) {
  console.log(chalk.red(`Command "${command}" not found.`), '\n')
  process.exit()
}

const exec = require(COMMAND_PATH)
exec(args)
