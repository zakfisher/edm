const defaultEvent = require('../models/event.json')

const event = {
  ...defaultEvent,
  slug: 'tomorrowland',
  title: 'Tomorrowland',
  image: 'https://cdn.assets.tomorrowland.com/2019/live/og2.jpg',
  website: 'https://www.tomorrowland.com',
  description: 'Live Today, Love Tomorrow, Unite Forever.',
  country: 'Belgium',
  keywords: []
}

const events = {}

events['2019'] = {
  event,
  startDate: new Date(2019, 6, 19),
  endDate: new Date(2019, 6, 29),
  sets: [
    'http://www.global-sets.com/dj-diesel-live-tomorrowland-belgium-week-1-19-jul-2019/',
    'http://www.global-sets.com/dj-snake-live-tomorrowland-belgium-week-1-20-jul-2019/'
  ]
}

events['2018'] = []

exports.events = events
