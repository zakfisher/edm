const fs = require('fs')
const shell = require('shelljs')

const ROOT_DIR = __dirname.replace('scripts', '')
const APP_DIR = __dirname.replace('scripts', 'app')
const FUNCTIONS_DIR = __dirname.replace('scripts', 'functions')
const SERVICES_DIR = __dirname + '/services'

// Create Next.js build + sym-links (in the functions folder)
shell.exec(`
  cd ${FUNCTIONS_DIR} &&
  rm -rf ./.next ./node_modules ./server ./services ./data &&
  ln -s ${ROOT_DIR}/node_modules ./node_modules &&
  ln -s ${APP_DIR}/.next ./.next &&
  ln -s ${APP_DIR}/server ./server &&
  ln -s ${SERVICES_DIR} ./services &&
  cd ${APP_DIR} &&
  FORCE_COLOR=1 next build
`)

// Overwrite functions/package.json
fs.writeFileSync(
  `${FUNCTIONS_DIR}/package.json`,
  JSON.stringify({
    ...require('../package.json'),
    scripts: {
      "serve": "firebase serve --functions-only",
      "shell": "firebase functions:shell",
      "start": "npm run shell",
      "deploy": "firebase deploy",
      "logs": "firebase functions:log"
    },
    engines: { node: "8" }
  }, null, 2)
)
