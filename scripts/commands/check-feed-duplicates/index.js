const chalk = require('chalk')
const feeds = require('../../data/podcast-feeds')

module.exports = (args = []) => {
  const feedMap = {}
  feeds.map((url, i) => {
    if (feedMap[url]) {
      console.log('Duplicate', i, url)
    }
    feedMap[url] = true
  })
}
