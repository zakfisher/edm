const chalk = require('chalk')
const feeds = require('../../data/blog-feeds')
const fetchBlog = require('./fetch-blog')

const fetchBlogs = () => new Promise((resolve, reject) => {
  const queue = feeds.map(feedUrl => () => fetchBlog(feedUrl))

  const fetchNextBlog = () => {
    const next = queue.shift()
    if (!next) return resolve()
    next()
      .then(fetchNextBlog)
      .catch(err => {
        console.log(chalk.red(err))
        fetchNextBlog()
      })
  }

  fetchNextBlog()
})

module.exports = fetchBlogs
