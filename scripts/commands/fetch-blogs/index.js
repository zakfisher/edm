const chalk = require('chalk')
const mongoose = require('mongoose')
mongoose.set('debug', true)
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')
const fetchBlogs = require('./fetch-blogs')

module.exports = () => {
  connectDB('v1', MONGO_URL)
    .then(fetchBlogs)
    .then(() => process.exit())
    .catch(err => {
      console.log(chalk.red(err))
    })
}
