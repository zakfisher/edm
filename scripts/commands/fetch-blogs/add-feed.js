const _ = require('lodash')
const fs = require('fs')
const chalk = require('chalk')
const shell = require('shelljs')
const slugify = require('../../services/utils/slugify')
const decodeEntities = require('../../services/utils/decode-entities')
const imagekit = require('../../services/imagekit/server')

const SCRIPTS_DIR = __dirname.split('/commands')[0]
const BLOGS_DIR = SCRIPTS_DIR + '/downloads/blogs'

const addFeed = (feedUrl, rss) => new Promise((resolve, reject) => {

  // Reset workspace
  console.log(chalk.gray('Resetting workspace...'))

  getFeed(feedUrl, rss)
    .then(feed => updateFeed(rss, feed))
    .then(feed => resolve([rss, feed]))
    .catch(reject)
})

const getFeed = (feedUrl, rss) => {
  // Get existing feed document
  console.log(chalk.gray('Checking for existing feed...'))
  const Feed = require('../../services/mongodb/models/Feed')
  return Feed.findOne({ feedUrl })
    .then(existingFeed => {
      if (existingFeed) return existingFeed

      // Create new feed
      console.log(chalk.gray('Creating new feed...'))
      const newFeed = new Feed({ __t: 'BLOG', feedUrl })
      const error = newFeed.validateSync()
      if (error) throw error

      return newFeed.save()
    })
}

const updateFeed = (rss, feed) => {
  // Update feed with latest RSS data
  console.log(chalk.gray('Updating feed with latest RSS data...'))
  feed.rss = rss
  feed.slug = slugify(rss.title)
  feed.title = decodeEntities(rss.title)
  feed.website = _.get(rss, 'link._', null)
    || _.get(rss, 'link', null)
  feed.description = decodeEntities((() => {
    return _.get(rss, 'description', null)
      || _.get(rss, 'googleplay:description', null)
      || _.get(rss, 'summary', null)
      || _.get(rss, 'itunes:subtitle', null)
  })())
  feed.lastUpdated = (() => {
    const items = _.get(rss, 'item', [])
    const firstItemDate = _.get(rss, 'item[0].pubDate', null)
    const lastItemDate = _.get(rss, `item[${items.length - 1}].pubDate`, null)
    const firstTime = firstItemDate ? (new Date(firstItemDate)).getTime() : 0
    const lastTime = lastItemDate ? (new Date(lastItemDate)).getTime() : 0
    const mostRecentDate = firstTime > lastTime ? firstItemDate : lastItemDate
    if (!mostRecentDate) return null
    return new Date(mostRecentDate)
  })()
  feed.lastFetched = new Date()
  feed.keywords = (() => {
    const keywords = _.get(rss, 'itunes:keywords', [])
    const keywordMap = {}
    feed.keywords.concat(keywords).forEach(keyword => keywordMap[keyword] = true)
    return Object.keys(keywordMap)
  })()

  const error = feed.validateSync()
  if (error) throw error
  return feed.save()
}

module.exports = addFeed
