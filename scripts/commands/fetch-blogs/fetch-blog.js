const parseRssFeed = require('../../services/utils/parse-rss-feed')
const addFeed = require('./add-feed')

const fetchBlog = feedUrl => {
  return parseRssFeed(feedUrl)
    .then(rss => addFeed(feedUrl, rss))
}

module.exports = fetchBlog
