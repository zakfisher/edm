const shell = require('shelljs')
const { HOST, DB, PORT } = require('../../services/mongodb/credentials/local.json')

const ROOT_DIR = __dirname.split('/scripts')[0]
const DUMP_DIR = ROOT_DIR + '/google-drive/db'
const DUMP = DUMP_DIR + '/edm-dev'

module.exports = () => {
  const deploy = [
    'mongorestore --drop',
    `--host ${HOST}`,
    `--port ${PORT}`,
    `--db ${DB}`,
    DUMP
  ].join(' ')
  shell.exec(deploy)
}
