const chalk = require('chalk')
const mongoose = require('mongoose')
mongoose.set('debug', true)
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')
const fetchPodcasts = require('../fetch-podcasts/fetch-podcasts')
const scrapePodcasts = require('../scrape-podcasts/scrape-podcasts')
const downloadPodcasts = require('../download-podcasts/download-podcasts')

module.exports = ([ index ]) => {
  connectDB('v1', MONGO_URL)
    .then(fetchPodcasts)
    .then(scrapePodcasts)
    .then(downloadPodcasts)
    .then(() => process.exit())
    .catch(err => {
      console.log(chalk.red(err))
    })
}
