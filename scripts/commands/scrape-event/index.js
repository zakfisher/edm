const fs = require('fs')
const chalk = require('chalk')
const shell = require('shelljs')
const _ = require('lodash')
const mongoose = require('mongoose')
const { JSDOM, VirtualConsole } = require('jsdom')
mongoose.set('debug', true)
const slugify = require('../../services/utils/slugify')
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')

const MONTHS = [
  'JAN',
  'FEB',
  'MAR',
  'APR',
  'MAY',
  'JUN',
  'JUL',
  'AUG',
  'SEP',
  'OCT',
  'NOV',
  'DEC'
]

const scrapeEvent = ([ keyword, type = 'OTHER' ]) => {
  connectDB('v1', MONGO_URL)
    .then(() => {
      const Entry = require('../../services/mongodb/models/Entry')
      const query = {}
      query.$or = [
        {
          'custom.keywords': keyword,
          'custom.hearThisDownloadUrl': { $ne: null }
        },
        {
          'custom.keywords': keyword,
          'custom.zippyShareDownloadUrl': { $ne: null }
        },
        {
          'custom.title': { $regex: keyword, $options: 'i' },
          'custom.hearThisDownloadUrl': { $ne: null }
        },
        {
          'custom.title': { $regex: keyword, $options: 'i' },
          'custom.zippyShareDownloadUrl': { $ne: null }
        },
      ]
      return Entry.find(query)
    })
    .then(entries => scrapeEntries(keyword, type, entries))
    .then(() => {
      process.exit()
    })
    .catch(err => {
      console.log(chalk.red(err))
      process.exit()
    })
}

const scrapeEntries = (keyword, type, entries) => {
  if (!entries.length) {
    console.log(chalk.red('no entries found'))
    return
  }

  const Event = require('../../services/mongodb/models/Event')
  const slug = slugify(keyword)

  const DOWNLOADS_DIR = __dirname.split('/commands')[0] + '/downloads'
  const EVENTS_DIR = DOWNLOADS_DIR + '/events'
  const DL_DIR = EVENTS_DIR + '/' + slug
  const LIVE_SETS_DIR = DL_DIR + '/live-sets'
  const EVENT_JSON = DL_DIR + '/event.json'

  shell.exec(`rm -rf ${DL_DIR} && mkdir -p ${LIVE_SETS_DIR}`)

  return Event.findOne({ slug })
    .then(existingEvent => {
      if (existingEvent) {
        return existingEvent
      }
      const newEvent = new Event({
        slug,
        title: keyword,
        __t: type.toUpperCase() // festival, club, radio, other
      })
      const error = newEvent.validateSync()
      if (error) throw error
      return newEvent
    })
    .then(event => new Promise(resolve => {

      fs.writeFileSync(
        EVENT_JSON,
        JSON.stringify(event, null, 2)
      )

      const queue = entries
        .sort((a, b) => a.custom.title > b.custom.title ? 1 : -1)
        .map(entry => scrapeEntry(entry, event))

      console.log(chalk.green(keyword))
      console.log(chalk.gray('Found'), entries.length, 'live sets')

      const scrapeNextEntry = () => {
        const scrape = queue.shift()
        if (!scrape) {
          return resolve()
        }
        scrape()
          .then(result => {
            const LIVE_SET_DIR = LIVE_SETS_DIR + '/' + result.doc.slug
            shell.exec(`mkdir ${LIVE_SET_DIR}`)
            fs.writeFileSync(
              LIVE_SET_DIR + '/data.json',
              JSON.stringify(result, null, 2)
            )
            scrapeNextEntry()
          })
          .catch(scrapeNextEntry)
      }

      scrapeNextEntry()
    }))
}

const scrapeEntry = (entry, event) => () => new Promise(resolve => {
  const Audio = require('../../services/mongodb/models/Audio')

  console.log(chalk.green(entry.custom.title))
  if (entry.custom.hearThisDownloadUrl) {
    console.log(entry.custom.hearThisDownloadUrl)
  }
  if (entry.custom.zippyShareDownloadUrl) {
    console.log(entry.custom.zippyShareDownloadUrl)
  }

  const dom = new JSDOM(entry.custom.content)

  // Attempt to parse date from title, otherwise fallback to entry date
  let date = entry.custom.date
  const titleDate = entry.custom.title.trim().substr(-11)
  const year = parseInt(titleDate.substr(-4))
  const month = MONTHS.indexOf(titleDate.substr(3, 3).toUpperCase())
  const day = parseInt(titleDate.substr(0, 2))
  const isValidYear = !isNaN(year) && year > 1900
  const isValidMonth = month > -1
  const isValidDate = !isNaN(day) && day > 0 && day < 32
  if (isValidYear && isValidMonth && isValidDate) {
    date = new Date(year, month, day)
  }

  let summary = null
  const summaryP = dom.window.document.querySelector('.entry p')
  if (summaryP) summary = summaryP.outerHTML

  const newAudio = new Audio({
    feed: entry.feed,
    entry: entry._id,
    event: event._id,
    slug: slugify(entry.custom.title),
    title: entry.custom.title,
    date,
    summary,
    website: entry.custom.url,
    keywords: entry.custom.keywords,
    audio: null,
    duration: null,
    size: null,
  })

  const error = newAudio.validateSync()
  if (error) throw error

  const result = {
    doc: newAudio,
    downloadUrl: entry.custom.hearThisDownloadUrl || entry.custom.zippyShareDownloadUrl,
  }

  setTimeout(() => resolve(result), 100)
})

module.exports = scrapeEvent
