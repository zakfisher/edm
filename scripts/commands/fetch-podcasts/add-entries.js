const _ = require('lodash')
const fs = require('fs')
const chalk = require('chalk')
const shell = require('shelljs')
const slugify = require('../../services/utils/slugify')
const decodeEntities = require('../../services/utils/decode-entities')
const feedly = require('../../services/feedly/server')

const addEntries = (feedUrl, rss, feed) => {
  return addRssEntries(rss, feed)
    .then(() => addFeedlyEntries(feedUrl, feed))
}

const addRssEntries = (rss, feed) => {
  const Entry = require('../../services/mongodb/models/Entry')
  console.log(chalk.gray('Importing RSS entries...'))

  const rssItems = _.get(rss, 'item', [])

  const slugs = {}
  rssItems.map(entry => {
    const slug = slugify(entry.title)
    entry.slug = slug
    slugs[slug] = entry
  })

  const getExistingEntries = () => {
    // Check for existing entries
    console.log(chalk.gray('Checking existing entries...'))
    return Entry.find({
      'rss.slug': { $in: Object.keys(slugs) }
    }, 'rss.slug')
  }

  const getNewEntries = (existingEntries) => {
    // Fetch new entries only
    console.log(chalk.gray('Filtering new entries...'))
    const existingSlugs = {}
    existingEntries.map(({ rss }) => existingSlugs[rss.slug] = 1)
    const newEntries = Object.keys(slugs)
      .filter(slug => !existingSlugs[slug])
      .map(slug => slugs[slug])
    return newEntries
  }

  const createEntryDocs = entries => {
    // Create new entry documents
    console.log(chalk.gray(`Creating ${chalk.white(entries.length + ' RSS entries')}...`))
    if (!entries.length) return Promise.resolve()

    return Promise.all(entries.map(entry => {
      const newEntry = new Entry({
        feed: feed._id,
        rss: entry
      })
      const error = newEntry.validateSync()
      if (error) throw error
      return newEntry.save()
    }))
  }

  return getExistingEntries()
    .then(getNewEntries)
    .then(createEntryDocs)
}

const addFeedlyEntries = (feedUrl, feed) => new Promise((resolve, reject) => {
  const Entry = require('../../services/mongodb/models/Entry')
  console.log(chalk.gray('Importing Feedly entries...'))

  // Import Feedly entries, 1000 at a time
  const getNextBatch = () => {
    feedly.getStream(feedUrl, 1000, feed.continuation)
      .then(getExistingEntries)
      .then(getNewEntries)
      .then(createEntryDocs)
      .then(updateFeed)
      .then(() => {
        if (feed.continuation) return getNextBatch()
        return resolve()
      })
      .catch(reject)
  }

  const getExistingEntries = res => {
    // Check for existing entries
    console.log(chalk.gray('Checking existing entries...'))
    const entryIds = _.get(res, 'data.ids', [])
    const continuation = _.get(res, 'data.continuation', null)
    return Promise.all([
      Entry.find({ 'feedly.id': { $in: entryIds } }, 'feedly.id'),
      entryIds,
      continuation
    ])
  }

  const getNewEntries = ([existingEntries, entryIds, continuation]) => {
    // Fetch new entries only
    console.log(chalk.gray('Filtering new entries...'))
    const existingIds = {}
    existingEntries.map(({ feedly }) => existingIds[feedly.id] = 1)
    const newIds = entryIds.filter(id => !existingIds[id])
    feed.continuation = newIds.length ? continuation : null
    return feedly.getEntries(newIds)
  }

  const createEntryDocs = res => {
    // Create new entry documents
    const entries = _.get(res, 'data', [])
    console.log(chalk.gray(`Creating ${chalk.white(entries.length + ' Feedly entries')}...`))
    if (!entries.length) return resolve([])
    return Promise.all(entries.map(entry => {
      // Check for RSS duplicate
      return Entry.findOne({
        feed: feed._id,
        'rss.slug': slugify(entry.title)
      }, 'rss.slug')
        .then(duplicateEntry => {
          if (duplicateEntry) {
            console.log(chalk.yellow('RSS duplicate'), duplicateEntry.rss.slug)
            return
          }

          const newEntry = new Entry({
            feed: feed._id,
            feedly: entry
          })
          const error = newEntry.validateSync()
          if (error) throw error
          return newEntry.save()
        })
    }))
  }

  const updateFeed = newEntries => {
    console.log(chalk.gray('Updating feed continuation'))
    return feed.save()
  }

  getNextBatch()
})

module.exports = addEntries
