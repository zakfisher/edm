const parseRssFeed = require('../../services/utils/parse-rss-feed')
const addFeed = require('./add-feed')
const addEntries = require('./add-entries')

const fetchPodcast = feedUrl => {
  return parseRssFeed(feedUrl)
    .then(rss => addFeed(feedUrl, rss))
    .then(([rss, feed]) => addEntries(feedUrl, rss, feed))
}

module.exports = fetchPodcast
