const chalk = require('chalk')
const feeds = require('../../data/podcast-feeds')
const fetchPodcast = require('./fetch-podcast')

const fetchPodcasts = () => new Promise((resolve, reject) => {
  const queue = feeds.map(feedUrl => () => fetchPodcast(feedUrl))

  const fetchNextPodcast = () => {
    const next = queue.shift()
    if (!next) return resolve()
    next()
      .then(fetchNextPodcast)
      .catch(err => {
        console.log(chalk.red(err))
        fetchNextPodcast()
      })
  }

  fetchNextPodcast()
})

module.exports = fetchPodcasts
