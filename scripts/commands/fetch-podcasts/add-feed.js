const _ = require('lodash')
const fs = require('fs')
const chalk = require('chalk')
const shell = require('shelljs')
const slugify = require('../../services/utils/slugify')
const decodeEntities = require('../../services/utils/decode-entities')
const imagekit = require('../../services/imagekit/server')
const downloadFile = require('../../services/utils/download-file')

const SCRIPTS_DIR = __dirname.split('/commands')[0]
const PODCASTS_DIR = SCRIPTS_DIR + '/downloads/podcasts'

const addFeed = (feedUrl, rss) => new Promise((resolve, reject) => {
  if (!rss) return resolve()

  // Reset workspace
  console.log(chalk.gray('Resetting workspace...'))
  const slug = slugify(rss.title)

  getFeed(feedUrl, rss)
    .then(([feed, imageUrl]) => updateFeed(rss, feed, imageUrl))
    .then(feed => resolve([rss, feed]))
    .catch(reject)
})

const getFeed = (feedUrl, rss) => {
  // Get existing feed document
  console.log(chalk.gray('Checking for existing feed...'))
  const Feed = require('../../services/mongodb/models/Feed')
  return Feed.findOne({ feedUrl })
    .then(existingFeed => {
      if (existingFeed) {
        return Promise.all([
          existingFeed,
          existingFeed.image ? null : extractImage(rss)
        ])
      }

      // Create new feed
      console.log(chalk.gray('Creating new feed...'))
      const newFeed = new Feed({ __t: 'PODCAST', feedUrl })
      const error = newFeed.validateSync()
      if (error) throw error

      return Promise.all([
        newFeed.save(),
        extractImage(rss)
      ])
    })
}

const updateFeed = (rss, feed, image) => {
  // Update feed with latest RSS data
  console.log(chalk.gray('Updating feed with latest RSS data...'))
  feed.rss = rss
  feed.slug = slugify(rss.title)
  feed.title = decodeEntities(rss.title)
  feed.website = _.get(rss, 'link._', null)
    || _.get(rss, 'link', null)
  feed.description = decodeEntities((() => {
    return _.get(rss, 'description', null)
      || _.get(rss, 'googleplay:description', null)
      || _.get(rss, 'summary', null)
      || _.get(rss, 'itunes:subtitle', null)
  })())
  if (image) {
    feed.image = image
  }
  feed.lastUpdated = (() => {
    const items = _.get(rss, 'item', [])
    const firstItemDate = _.get(rss, 'item[0].pubDate', null)
    const lastItemDate = _.get(rss, `item[${items.length - 1}].pubDate`, null)
    const firstTime = firstItemDate ? (new Date(firstItemDate)).getTime() : 0
    const lastTime = lastItemDate ? (new Date(lastItemDate)).getTime() : 0
    const mostRecentDate = firstTime > lastTime ? firstItemDate : lastItemDate
    if (!mostRecentDate) return null
    return new Date(mostRecentDate)
  })()
  feed.lastFetched = new Date()
  feed.keywords = (() => {
    const keywords = _.get(rss, 'itunes:keywords', [])
    const keywordMap = {}
    feed.keywords.concat(keywords).forEach(keyword => keywordMap[keyword] = true)
    return Object.keys(keywordMap).join(',').split(',')
  })()

  const error = feed.validateSync()
  if (error) throw error
  return feed.save()
}

const extractImage = rss => new Promise((resolve, reject) => {
  console.log(chalk.gray('Downloading feed image...'))
  const imageSrc = 'https://is4-ssl.mzstatic.com/image/thumb/Podcasts71/v4/fb/08/ff/fb08ff00-0102-e736-f796-8017f6697616/mza_3646323640852653417.jpg/1000x0w.jpg'
  // const imageSrc = _.get(rss, 'image.url', null)
  //   || _.get(rss, '["itunes:image"].href', null)
  if (!imageSrc) return reject('Feed image not found')

  const slug = slugify(rss.title)
  const IMAGE_DIR = `${PODCASTS_DIR}/${slug}/image`
  shell.exec(`mkdir -p ${IMAGE_DIR}`)
  downloadFile(imageSrc, IMAGE_DIR)
    .then(() => {
        // Upload the image to imagekit
        const IMAGE_PATH = IMAGE_DIR + '/' + imageSrc.split('/').pop()
        const ext = IMAGE_PATH.split('.').pop()
        if (!fs.existsSync(IMAGE_PATH)) return resolve()
        const file = fs.readFileSync(IMAGE_PATH, 'base64')

        console.log(chalk.gray('Uploading feed image...'))
        return imagekit.upload(file, {
          filename: `${slug}.${ext}`,
          folder: '/podcasts'
        })
    })
    .then(res => {
      console.log(res.url)
      resolve(res)
    })
    .catch(reject)
})

module.exports = addFeed
