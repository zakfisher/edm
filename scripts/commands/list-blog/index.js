const chalk = require('chalk')
const feeds = require('../../data/blog-feeds')

module.exports = ([ index ]) => {
  const feedUrl = feeds[index] || null

  if (!feedUrl) {
    console.log(chalk.red(`Feed URL not found for blog "${index}".`))
    console.log(chalk.gray(`Run ${chalk.white('nr e list-blogs')} to show all indexes.`))
    console.log()
    process.exit()
  }

  console.log(chalk.green('BLOG ' + index))
  console.log(feedUrl)
  console.log()
}
