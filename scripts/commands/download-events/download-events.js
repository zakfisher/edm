require('events').EventEmitter.defaultMaxListeners = 15

const _ = require('lodash')
const chalk = require('chalk')
const fs = require('fs')
const shell = require('shelljs')
const mm = require('music-metadata')
const backblaze = require('../../services/backblaze/server')
const downloadFile = require('../../services/utils/download-file')

const SCRIPTS_DIR = __dirname.split('/commands')[0]
const EVENTS_DIR = SCRIPTS_DIR + '/downloads/events'
const events = fs.readdirSync(EVENTS_DIR).filter(n => n[0] !== '.')

const downloadEvents = () => new Promise(resolve => {
  console.log(chalk.gray('Downloading events...'))
  const queue = events.map(slug => () => downloadEvent(slug))

  const downloadNextEvent = () => {
    const next = queue.shift()
    if (!next) return resolve()
    next()
      .then(downloadNextEvent)
      .catch(err => {
        console.log(chalk.red(err))
        downloadNextEvent()
      })
  }

  downloadNextEvent()
})

const downloadEvent = slug => {
  const Event = require('../../services/mongodb/models/Event')
  const EVENT_DIR = `${EVENTS_DIR}/${slug}`
  const LIVE_SETS_DIR = EVENT_DIR + '/live-sets'
  const EVENT_JSON = EVENT_DIR + '/event.json'

  const liveSets = fs.readdirSync(LIVE_SETS_DIR)
    .filter(n => n[0] !== '.')
    .map(slug => `${LIVE_SETS_DIR}/${slug}/data.json`)

  return Event.findOne({ slug })
    .then(existingEvent => {
      if (existingEvent) {
        return existingEvent
      }
      const newEvent = new Event(require(EVENT_JSON))
      const error = newEvent.validateSync()
      if (error) throw error
      return newEvent.save()
    })
    .then(entry => {
      shell.exec(`rm -rf ${EVENT_JSON}`)
      return new Promise(resolve => {
        const queue = liveSets.map(path => () => downloadAudio(path))

        let currentRequests = 0
        const parallelRequests = 10

        const downloadNextAudio = () => {

          // Limit concurrent requests
          if (currentRequests === parallelRequests) return

          const next = queue.shift()
          if (!next) {
            if (currentRequests === 0) resolve()
            return
          }

          currentRequests++
          next()
            .then(() => {
              currentRequests--
              downloadNextAudio()
            })
            .catch(err => {
              console.log(chalk.red(err))
              downloadNextAudio()
            })

          // Check for the next one immediately
          downloadNextAudio()
        }

        downloadNextAudio()
      })
    })
}

const downloadAudio = path => new Promise(resolve => {
  const Event = require('../../services/mongodb/models/Event')
  const Entry = require('../../services/mongodb/models/Entry')
  const Audio = require('../../services/mongodb/models/Audio')

  const { doc, downloadUrl } = require(path)
  const LIVE_SETS_DIR = path.split('live-sets')[0] + 'live-sets'
  const LIVE_SET_DIR = path.replace('/data.json', '')
  const AUDIO_DIR = path.replace('data.json', 'audio')
  const SRC = AUDIO_DIR.split('/downloads/').pop()
  const FILESIZE_MIN = 20 * 1024 * 1024 // MB

  console.log(
    chalk.cyan('[DOWNLOAD]'),
    chalk.yellow(doc.title)
  )

  // Download audio file
  shell.exec(`rm -rf ${AUDIO_DIR}`)
  Promise.all([
    Event.findOne({ _id: doc.event }, 'slug'),
    Entry.findOne({ _id: doc.entry }, 'slug').populate('feed', 'slug'),
    Audio.findOne({
      event: doc.event,
      slug: doc.slug
    })
  ])
    .then(([ event, entry, existingAudio ]) => {
      if (!event) throw new Error('Event not found')
      if (!entry) throw new Error('Entry not found')

      // If we already have this file, update the entry flags and escape
      const existingAudioSize = _.get(existingAudio, 'size', 0)
      if (existingAudio && existingAudioSize > 0) {
        entry.keywords = doc.keywords
        entry.website = doc.website
        entry.downloadSuccess = true
        entry.uploadSuccess = true
        return entry.save().then(() => {
          throw new Error('Duplicate entry')
        })
      }

      const next = () => Promise.all([
        event,
        entry,
        downloadFile(downloadUrl, AUDIO_DIR)
      ])

      // If we already have this file but it is corrupt, re-download it
      if (existingAudio && existingAudioSize <= FILESIZE_MIN) {
        console.log(chalk.red('fix audio'), doc.title)
        return existingAudio.remove().then(next)
      }

      return next()
    })
    .then(([event, entry]) => {
      // Handle download success
      const filename = fs.existsSync(AUDIO_DIR) && (fs.readdirSync(AUDIO_DIR)[0] || null)
      if (filename) {
        entry.downloadSuccess = true
        const formattedFileName = filename.split('?')[0]

        if (filename.includes('?')) {
          shell.exec(`mv "${AUDIO_DIR}/${filename}" "${AUDIO_DIR}/${formattedFileName}"`)
        }
        const fileSrc = `${AUDIO_DIR}/${formattedFileName}`

        const size = fs.statSync(fileSrc).size
        if (size < FILESIZE_MIN) {
          entry.downloadError = true
          throw new Error('File size is too small to upload')
        }
        return uploadAudio(event, entry, doc, fileSrc)
      }

      // Handle download error
      else {
        console.log(chalk.red('Download error'), SRC)
        cleanUp(LIVE_SET_DIR)
        entry.downloadError = true
        const error = entry.validateSync()
        if (error) throw error
        entry.save().then(resolve)
      }
    })
    .then(resolve)
    .catch(err => {
      cleanUp(LIVE_SET_DIR)
      console.log(chalk.red(err), SRC)
      resolve()
    })
})

const uploadAudio = (event, entry, doc, fileSrc) => new Promise(resolve => {
  const Audio = require('../../services/mongodb/models/Audio')

  const filename = fileSrc.split('/').pop()
  const ext = filename.split('.').pop()
  const fileDest = `events/${event.slug}/${doc.slug}.${ext}`
  const LIVE_SET_DIR = fileSrc.split('/audio')[0]
  console.log(chalk.cyan('[UPLOAD]'), chalk.yellow(doc.title))

  // Get audio file stats
  doc.size = fs.statSync(fileSrc).size
  if (doc.size === 0) {
    throw new Error('File size is zero')
  }

  mm.parseFile(fileSrc, { native: true })
    .then(meta => {
      // Set duration (extracted from audio file)
      doc.duration = _.get(meta, 'format.duration', doc.duration)

      // Upload file to B2
      return backblaze.uploadFile(fileSrc, fileDest)
    })
    .then(url => {
      console.log(url)
      entry.uploadSuccess = true
      doc.audio = url

      // Create new audio document
      const newAudio = new Audio(doc)
      let error = newAudio.validateSync()
      if (!error) error = entry.validateSync()
      if (error) throw error

      return Promise.all([
        entry.save(),
        newAudio.save()
      ])
    })
    .then(() => {
      cleanUp(LIVE_SET_DIR)
      resolve()
    })
    .catch(err => {
      cleanUp(LIVE_SET_DIR)
      entry.uploadError = true
      const error = entry.validateSync()
      if (error) throw error
      entry.save().then(resolve)
    })
})

const cleanUp = LIVE_SET_DIR => {
  shell.exec(`rm -rf ${LIVE_SET_DIR}`)
  const LIVE_SETS_DIR = LIVE_SET_DIR.split('/').slice(0, -1).join('/')
  if (fs.existsSync(LIVE_SETS_DIR) && !fs.readdirSync(LIVE_SETS_DIR).length) {
    shell.exec(`rm -rf ${LIVE_SETS_DIR}`)
  }
  const EVENT_DIR = LIVE_SETS_DIR.split('/').slice(0, -1).join('/')
  if (fs.existsSync(EVENT_DIR) && !fs.readdirSync(EVENT_DIR).length) {
    shell.exec(`rm -rf ${EVENT_DIR}`)
  }
}

module.exports = {
  downloadEvents,
  downloadEvent
}
