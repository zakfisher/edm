const chalk = require('chalk')
const mongoose = require('mongoose')
mongoose.set('debug', true)
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')
const { fetchBlog } = require('../fetch/fetch-blogs')
const feeds = require('../../data/blog-feeds')

module.exports = ([ index ]) => {
  const feedUrl = feeds[index] || null

  if (!feedUrl) {
    console.log(chalk.red(`Feed URL not found for blog "${index}".`))
    console.log(chalk.gray(`Run ${chalk.white('nr e list-blogs')} to show all indexes.`))
    console.log()
    process.exit()
  }

  connectDB('v1', MONGO_URL)
    .then(() => fetchBlog(feedUrl))
    .then(() => process.exit())
    .catch(err => {
      console.log(chalk.red(err))
    })
}
