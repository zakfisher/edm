const chalk = require('chalk')
const mongoose = require('mongoose')
mongoose.set('debug', true)
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')
const fetchPodcast = require('../fetch-podcasts/fetch-podcast')
const feeds = require('../../data/podcast-feeds')

module.exports = ([ index ]) => {
  const feedUrl = feeds[index] || null

  if (!feedUrl) {
    console.log(chalk.red(`Feed URL not found for podcast "${index}".`))
    console.log(chalk.gray(`Run ${chalk.white('nr e list-podcasts')} to show all indexes.`))
    console.log()
    process.exit()
  }

  connectDB('v1', MONGO_URL)
    .then(() => fetchPodcast(feedUrl))
    .then(() => process.exit())
    .catch(err => {
      console.log(chalk.red(err))
    })
}
