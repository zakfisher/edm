const chalk = require('chalk')
const mongoose = require('mongoose')
mongoose.set('debug', true)
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')

module.exports = () => {
  connectDB('v1', MONGO_URL)
    .then(() => new Promise((resolve, reject) => {
      const Feed = require('../../services/mongodb/models/Feed')
      const Entry = require('../../services/mongodb/models/Entry')

      Feed.find({ __t: 'BLOG' })
        .then(feeds => {
          // Delete blogs
          const blogIds = feeds.map(f => f._id)
          return Promise.all([blogIds].concat(feeds.map(f => f.remove())))
        })
        .then(([blogIds]) => {
          // Delete all blog entries
          const resetNextBatch = () => {
            Entry.find({ feed: { $in: blogIds } })
              .limit(1000)
              .then(entries => {
                if (!entries.length) return resolve()
                console.log(chalk.gray(`Deleting ${entries.length} entries...`))
                return Promise.all(entries.map(entry => entry.remove()))
              })
              .then(resetNextBatch)
              .catch(err => {
                console.log(chalk.red(err))
                resetNextBatch()
              })
          }
          resetNextBatch()
        })
    }))
    .then(() => process.exit())
    .catch(err => {
      console.log(chalk.red(err))
    })
}
