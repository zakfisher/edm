const chalk = require('chalk')
const scrapeEntry = require('./scrape-entry')

const scrapePodcasts = () => new Promise(resolve => {
  const Feed = require('../../services/mongodb/models/Feed')
  const Entry = require('../../services/mongodb/models/Entry')

  console.log(chalk.gray('Scraping podcasts...'))

  Feed.find({ __t: 'PODCAST' }, '_id slug')
    .then(feeds => {
      const podcastFeedIds = feeds.map(({ _id }) => _id)

      const scrapeNextBatch = () => {
        Entry.find({
          feed: { $in: podcastFeedIds },
          scrapeSuccess: { $ne: true },
          scrapeError: { $ne: true },
        }).populate('feed', 'slug').limit(100)
        .then(entries => {
          if (!entries.length) return null
          return Promise.all(
            entries.map(entry => scrapeEntry(entry.feed, entry).then(() => entry))
          )
        })
        .then(entries => {
          if (!entries || !entries.length) return resolve()
          return Promise.all(entries.map(entry => entry.save()))
        })
        .then(scrapeNextBatch)
        .catch(err => {
          console.log(err)
          resolve()
        })
      }

      scrapeNextBatch()
    })
})

module.exports = scrapePodcasts
