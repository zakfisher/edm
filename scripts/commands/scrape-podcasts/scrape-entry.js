const _ = require('lodash')
const fs = require('fs')
const shell = require('shelljs')
const chalk = require('chalk')
const decodeEntities = require('../../services/utils/decode-entities')
const slugify = require('../../services/utils/slugify')

const SCRIPTS_DIR = __dirname.split('/commands')[0]
const DOWNLOADS_DIR = SCRIPTS_DIR + '/downloads'
const PODCASTS_DIR = DOWNLOADS_DIR + '/podcasts'

const scrapeEntry = (feed, entry) => new Promise(resolve => {
  const Audio = require('../../services/mongodb/models/Audio')

  const title = decodeEntities(
    _.get(entry, 'rss.title', null)
    || _.get(entry, 'feedly.title', '')
  )

  console.log(chalk.cyan('[SCRAPE]'), chalk.yellow(title))

  // Get download url
  const audioSrc = _.get(entry, 'rss.enclosure.url', '')
    || _.get(entry, 'rss.enclosure.href', '')
    || _.get(entry, 'rss.enclosure[0].href', null)
    || _.get(entry, 'feedly.enclosure.url', '')
    || _.get(entry, 'feedly.enclosure.href', '')
    || _.get(entry, 'feedly.enclosure[0].href', null)
  if (!audioSrc) {
    console.log(chalk.red('No download url'))
    entry.scrapeError = true
    return resolve()
  }

  let duration = _.get(entry, 'feedly.enclosure[0].length', null)
  if (duration) duration /= 60000

  // Create new audio document
  const slug = slugify(title)
  const date = new Date(
    _.get(entry, 'rss.pubDate', null)
    || _.get(entry, 'feedly.published', 0)
  )
  const newAudio = new Audio({
    feed: feed._id,
    entry: entry._id,
    slug,
    title,
    date,
    duration,
    summary: _.get(entry, 'rss["itunes:summary"]', null)
      || _.get(entry, 'feedly.summary.content', null),
    website: (() => {
      const website = _.get(entry, 'rss.link', null)
      || _.get(entry, 'feedly.originId', null)
      || _.get(entry, 'rss.guid._', null)
      || (_.get(entry, 'rss.guid', null) && typeof entry.rss.guid === 'string')
      if (typeof website === 'string' && website.indexOf('http') === 0) {
        return website
      }
      return null
    })(),
    keywords: _.get(entry, 'rss["itunes:keywords"]', null)
      || _.get(entry, 'rss.category', null)
      || _.get(entry, 'feedly.keywords', []),
  })

  const error = newAudio.validateSync()
  if (error) {
    console.log(chalk.red(error))
    return resolve()
  }

  // Create download directory
  console.log(audioSrc)
  const DL_DIR = `${PODCASTS_DIR}/${feed.slug}/${slug}`
  if (!fs.existsSync(DL_DIR)) {
    shell.exec(`mkdir -p ${DL_DIR}`)
  }

  // Write data.json
  fs.writeFileSync(
    `${DL_DIR}/data.json`,
    JSON.stringify({
      doc: newAudio,
      downloadUrl: audioSrc
    }, null, 2)
  )

  entry.scrapeSuccess = true
  resolve()
})

module.exports = scrapeEntry
