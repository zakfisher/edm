const chalk = require('chalk')
const feeds = require('../../data/podcast-feeds')

module.exports = (args = []) => {
  console.log(chalk.green('PODCASTS'))
  feeds.map((feed, i) => console.log(chalk.yellow(i), feed))
  console.log()
}
