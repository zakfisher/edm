const _ = require('lodash')
const chalk = require('chalk')
const fs = require('fs')
const shell = require('shelljs')
const downloadFile = require('../../services/utils/download-file')
const uploadAudio = require('./upload-audio')
const cleanUp = require('./clean-up')

const downloadAudio = path => new Promise(resolve => {
  const Entry = require('../../services/mongodb/models/Entry')
  const Audio = require('../../services/mongodb/models/Audio')

  const { doc, downloadUrl } = require(path)
  const EPISODE_DIR = path.replace('/data.json', '')
  const AUDIO_DIR = `${EPISODE_DIR}/audio`
  const SRC = AUDIO_DIR.split('/downloads/').pop()
  const FILESIZE_MIN = 1 * 1024 * 1024 // MB

  console.log(
    chalk.cyan('[DOWNLOAD]'),
    chalk.yellow(doc.title)
  )

  const getEntry = () => Promise.all([
    Entry.findOne({ _id: doc.entry }, 'slug').populate('feed', 'slug'),
    Audio.findOne({ feed: doc.feed, slug: doc.slug })
  ])

  const startDownload = ([ entry, existingAudio ]) => {
    if (!entry) throw new Error('Entry not found')

    // If we already have this file, update the entry flags and escape
    const existingAudioSize = _.get(existingAudio, 'size', 0)
    if (existingAudio && existingAudioSize > 0) {
      entry.keywords = doc.keywords
      entry.website = doc.website
      entry.downloadSuccess = true
      entry.uploadSuccess = true
      return entry.save().then(() => {
        throw new Error('Duplicate entry')
      })
    }

    const next = () => Promise.all([
      entry,
      downloadFile(downloadUrl, AUDIO_DIR)
    ])

    // If we already have this file but it is corrupt, re-download it
    if (existingAudio && existingAudioSize <= FILESIZE_MIN) {
      console.log(chalk.red('fix audio'), doc.title)
      return existingAudio.remove().then(next)
    }

    return next()
  }

  const handleResponse = ([entry]) => {
    // Handle download success
    const filename = fs.existsSync(AUDIO_DIR) && (fs.readdirSync(AUDIO_DIR)[0] || null)
    if (filename) {
      entry.downloadSuccess = true
      const formattedFileName = filename.split('?')[0]
      if (filename.includes('?')) {
        shell.exec(`mv ${AUDIO_DIR}/${filename} ${AUDIO_DIR}/${formattedFileName}`)
      }
      const fileSrc = `${AUDIO_DIR}/${formattedFileName}`
      const size = fs.statSync(fileSrc).size
      if (size < FILESIZE_MIN) {
        entry.downloadError = true
        throw new Error('File size is too small to upload')
      }
      return uploadAudio(entry, doc, fileSrc)
    }

    // Handle download error
    else {
      console.log(chalk.red('Download error'), SRC)
      cleanUp(EPISODE_DIR)
      entry.downloadError = true
      const error = entry.validateSync()
      if (error) throw error
      entry.save().then(resolve)
    }
  }

  // Download audio file
  shell.exec(`rm -rf ${AUDIO_DIR}`)
  getEntry()
    .then(startDownload)
    .then(handleResponse)
    .then(resolve)
    .catch(err => {
      cleanUp(EPISODE_DIR)
      console.log(chalk.red(err), SRC)
      resolve()
    })
})

module.exports = downloadAudio
