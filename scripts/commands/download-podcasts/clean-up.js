const fs = require('fs')
const shell = require('shelljs')

const cleanUp = EPISODE_DIR => {
  shell.exec(`rm -rf ${EPISODE_DIR}`)
  const PODCAST_DIR = EPISODE_DIR.split('/').slice(0, -1).join('/')
  if (!fs.readdirSync(PODCAST_DIR).length) {
    shell.exec(`rm -rf ${PODCAST_DIR}`)
  }
}

module.exports = cleanUp
