const chalk = require('chalk')
const fs = require('fs')
const downloadAudio = require('./download-audio')

const SCRIPTS_DIR = __dirname.split('/commands')[0]
const PODCASTS_DIR = SCRIPTS_DIR + '/downloads/podcasts'

const downloadEpisodes = podcastSlug => new Promise(resolve => {
  const PODCAST_DIR = `${PODCASTS_DIR}/${podcastSlug}`

  const data = fs.readdirSync(PODCAST_DIR)
    .filter(n => n[0] !== '.')
    .map(episodeSlug => `${PODCAST_DIR}/${episodeSlug}/data.json`)

  const queue = data.map(path => () => downloadAudio(path))

  let currentRequests = 0
  const parallelRequests = 10

  const downloadNextEpisode = () => {

    // Limit concurrent requests
    if (currentRequests === parallelRequests) return

    const next = queue.shift()
    if (!next) {
      if (currentRequests === 0) resolve()
      return
    }

    currentRequests++
    next()
      .then(() => {
        currentRequests--
        downloadNextEpisode()
      })
      .catch(err => {
        console.log(chalk.red(err))
        downloadNextEpisode()
      })

    // Check for the next one immediately
    downloadNextEpisode()
  }

  downloadNextEpisode()
})

module.exports = downloadEpisodes
