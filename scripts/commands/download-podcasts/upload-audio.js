const _ = require('lodash')
const chalk = require('chalk')
const fs = require('fs')
const mm = require('music-metadata')
const backblaze = require('../../services/backblaze/server')
const cleanUp = require('./clean-up')

const uploadAudio = (entry, doc, fileSrc) => new Promise(resolve => {
  const Audio = require('../../services/mongodb/models/Audio')

  const podcastSlug = entry.feed.slug
  const filename = fileSrc.split('/').pop()
  const fileDest = `podcasts/${podcastSlug}/${filename}`
  const EPISODE_DIR = fileSrc.split('/audio')[0]
  console.log(chalk.cyan('[UPLOAD]'), chalk.yellow(doc.title))

  // Get audio file stats
  doc.size = fs.statSync(fileSrc).size
  if (doc.size === 0) {
    throw new Error('File size is zero')
  }

  mm.parseFile(fileSrc, { native: true })
    .then(meta => {
      // Set duration (extracted from audio file)
      doc.duration = _.get(meta, 'format.duration', doc.duration)

      // Upload file to B2
      return backblaze.uploadFile(fileSrc, fileDest)
    })
    .then(url => {
      console.log(url)
      entry.uploadSuccess = true
      doc.audio = url

      // Create new audio document
      const newAudio = new Audio(doc)
      let error = newAudio.validateSync()
      if (!error) error = entry.validateSync()
      if (error) throw error

      return Promise.all([
        entry.save(),
        newAudio.save()
      ])
    })
    .then(() => {
      cleanUp(EPISODE_DIR)
      resolve()
    })
    .catch(err => {
      cleanUp(EPISODE_DIR)
      entry.uploadError = true
      const error = entry.validateSync()
      if (error) throw error
      entry.save().then(resolve)
    })
})

module.exports = uploadAudio
