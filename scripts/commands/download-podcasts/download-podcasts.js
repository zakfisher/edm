const chalk = require('chalk')
const fs = require('fs')
const downloadEpisodes = require('./download-episodes')

const SCRIPTS_DIR = __dirname.split('/commands')[0]
const PODCASTS_DIR = SCRIPTS_DIR + '/downloads/podcasts'

const downloadPodcasts = () => new Promise(resolve => {
  console.log(chalk.gray('Downloading podcasts...'))

  const podcasts = fs.readdirSync(PODCASTS_DIR).filter(n => n[0] !== '.')
  const queue = podcasts.map(slug => () => downloadEpisodes(slug))

  // Download one podcast at a time
  const downloadNextPodcast = () => {
    const next = queue.shift()
    if (!next) return resolve()
    next()
      .then(downloadNextPodcast)
      .catch(err => {
        console.log(chalk.red(err))
        downloadNextPodcast()
      })
  }

  downloadNextPodcast()
})

module.exports = downloadPodcasts
