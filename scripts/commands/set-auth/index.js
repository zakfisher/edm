const backblaze = require('../../services/backblaze/server')
const admin = require('../../services/firebase/server')
const db = admin.firestore()

module.exports = () => {
  backblaze.getDownloadAuth()
    .then(token => {
      db.collection('admin').doc('backblaze').set({
        token
      })
    })
    .catch(console.error)
}
