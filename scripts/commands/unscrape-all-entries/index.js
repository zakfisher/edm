const chalk = require('chalk')
const mongoose = require('mongoose')
mongoose.set('debug', true)
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')

module.exports = () => {
  connectDB('v1', MONGO_URL)
    .then(() => new Promise((resolve, reject) => {
      const Entry = require('../../services/mongodb/models/Entry')

      const resetNextBatch = () => {
        Entry.find({
          $or: [
            { scrapeSuccess: { $ne: null } },
            { scrapeError: { $ne: null } },
            { downloadSuccess: { $ne: null } },
            { downloadError: { $ne: null } },
            { uploadSuccess: { $ne: null } },
            { uploadError: { $ne: null } },
          ]
        })
          .limit(1000)
          .then(entries => {
            if (!entries.length) return resolve()
            console.log(chalk.gray(`Resetting ${entries.length} entries...`))
            return Promise.all(entries.map(entry => {
              entry.scrapeSuccess = null
              entry.scrapeError = null
              entry.downloadSuccess = null
              entry.downloadError = null
              entry.uploadSuccess = null
              entry.uploadError = null
              return entry.save()
            }))
          })
          .then(resetNextBatch)
          .catch(err => {
            console.log(chalk.red(err))
            resetNextBatch()
          })
      }

      resetNextBatch()
    }))
    .then(() => process.exit())
    .catch(err => {
      console.log(chalk.red(err))
    })
}
