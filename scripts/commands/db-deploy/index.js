const shell = require('shelljs')
const { HOST, DB, USER, PASS } = require('../../services/mongodb/credentials/prod.json')

const ROOT_DIR = __dirname.split('/scripts')[0]
const DUMP_DIR = ROOT_DIR + '/google-drive/db'
const DUMP = DUMP_DIR + '/edm-dev'

module.exports = ([ drop = false ]) => {
  const deploy = [
    'mongorestore',
    `--nsExclude "${DB}.entries"`,
    drop ? '--drop' : '',
    `--host ${HOST}`,
    `--db ${DB}`,
    `--ssl`,
    `--username ${USER}`,
    `--password ${PASS}`,
    `--authenticationDatabase admin ${DUMP}`,
  ].join(' ')
  shell.exec(deploy)
}
