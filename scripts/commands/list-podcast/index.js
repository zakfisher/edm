const chalk = require('chalk')
const feeds = require('../../data/podcast-feeds')

module.exports = ([ index ]) => {
  const feedUrl = feeds[index] || null

  if (!feedUrl) {
    console.log(chalk.red(`Feed URL not found for podcast "${index}".`))
    console.log(chalk.gray(`Run ${chalk.white('nr e list-podcasts')} to show all indexes.`))
    console.log()
    process.exit()
  }

  console.log(chalk.green('PODCAST ' + index))
  console.log(feedUrl)
  console.log()
}
