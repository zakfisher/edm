const chalk = require('chalk')
const mongoose = require('mongoose')
mongoose.set('debug', true)
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')
const scrapeGlobalSets = require('../scrape-blogs/scrape-global-sets')

module.exports = ([ index ]) => {
  let scrapeIt = null
  if (index == 0) scrapeIt = scrapeGlobalSets
  // if (index == 1) scrapeIt = scrapeMixingDj

  if (!scrapeIt) {
    console.log(chalk.red(`Scraper not found for blog "${index}".`))
    process.exit()
  }

  connectDB('v1', MONGO_URL)
    .then(scrapeIt)
    .then(() => process.exit())
    .catch(err => {
      console.log(chalk.red(err))
    })
}
