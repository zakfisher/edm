const chalk = require('chalk')
const shell = require('shelljs')
const mongoose = require('mongoose')
mongoose.set('debug', true)
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')
const writeExistingEntries = require('./write-existing-entries')
const scrapeGlobalSets = require('./scrape-global-sets')
const getKeywords = require('../get-keywords/get-keywords')

module.exports = ([ index ]) => {
  connectDB('v1', MONGO_URL)
    .then(writeExistingEntries)
    .then(scrapeGlobalSets)
    .then(getKeywords)
    .then(() => process.exit())
    .catch(err => {
      console.log(chalk.red(err))
    })
}
