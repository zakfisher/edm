const fs = require('fs')
const moment = require('moment')
const { JSDOM, VirtualConsole } = require('jsdom')
const shell = require('shelljs')
const chalk = require('chalk')
const feeds = require('../../data/blog-feeds')
const decodeEntities = require('../../services/utils/decode-entities')
const slugify = require('../../services/utils/slugify')

let feedId = null
let feedSlug = null

module.exports = () => new Promise(resolve => {
  const feedUrl = feeds[0]
  const Feed = require('../../services/mongodb/models/Feed')

  // Get feed then curl the first list page
  Feed.findOne({ feedUrl }).then(feed => {
    if (!feed) throw new Error('feed not found')
    feedId = feed._id
    feedSlug = feed.slug
    return scrapeListPages(feed)
  })
  .then(resolve)
  .catch(err => {
    console.log(chalk.red(err))
    resolve()
  })
})

let currentPage = 0
const query = '?s=live'
const getNextUrl = () => {
  return ++currentPage === 1
    ? `http://www.global-sets.com/${query}`
    : `http://www.global-sets.com/page/${currentPage}/${query}`
}

const MAX_EMPTY_LIST_PAGES = 2
let listPagesWithZeroNewItems = 0

const scrapeListPages = feed => new Promise(resolve => {

  const scrapeNextListPage = () => {
    const nextUrl = getNextUrl()
    const { items, lastUrl } = scrapeListPage(nextUrl)

    const next = () => {
      const maxNoResults = listPagesWithZeroNewItems === MAX_EMPTY_LIST_PAGES
      if (maxNoResults || nextUrl === lastUrl) {
        return resolve()
      }
      scrapeNextListPage()
    }

    scrapeItemPages(items)
      .then(next)
      .catch(next)
  }

  scrapeNextListPage()
})

const scrapeListPage = url => {
  console.log(chalk.cyan('[SCRAPE]'), url)
  const html = shell.exec(`curl ${url}`, { silent: true })
  const dom = new JSDOM(html)
  const listItems = dom.window.document.querySelectorAll('article.item-list')
  if (!listItems || !listItems.length) {
    console.log(chalk.red('No list items found'), url)
  }
  const icons = dom.window.document.querySelectorAll('i.fa')
  icons.forEach(icon => icon.remove())
  const items = []
  listItems.forEach(item => {
    const title = decodeEntities(item.querySelector('h3.post-box-title a').innerHTML)
    const slug = slugify(title)
    items.push({
      slug,
      title,
      url: item.querySelector('h3.post-box-title a').getAttribute('href'),
      date: new Date(item.querySelector('span.tie-date').innerHTML),
      keywords: [...item.querySelectorAll('span.post-cats a')].map(a => decodeEntities(a.innerHTML))
    })
  })
  const lastUrl = dom.window.document.querySelector("div.pagination > a.last").getAttribute('href')
  return { items, lastUrl }
}

const scrapeItemPages = items => new Promise(resolve => {
  const SLUGS = __dirname + '/existing-slugs.json'
  const existingSlugs = fs.existsSync(SLUGS) ? require(SLUGS) : []

  // Scrape each item page, one at a time
  const newEntries = items
    .filter(item => !existingSlugs.includes(item.slug))
    .map(item => () => scrapeItemPage(item))

  if (!newEntries.length) {
    console.log(chalk.red('No new entries'))
    listPagesWithZeroNewItems++
  }

  const scrapeNextItemPage = () => {
    const scrapeNextPage = newEntries.shift()
    if (!scrapeNextPage) return resolve()
    scrapeNextPage()
      .then(scrapeNextItemPage)
      .catch(scrapeNextItemPage)
  }

  scrapeNextItemPage()
})

const scrapeItemPage = item => new Promise(resolve => {
  console.log(chalk.cyan('[SCRAPE]'), item.url)

  const html = shell.exec(`curl ${item.url}`, { silent: true })
  const dom = new JSDOM(html)

  // Create entry with raw html
  const { head, body } = dom.window.document
  // const keywords = head.querySelector('meta[name="keywords"]').getAttribute('content')
  const content = body.querySelector('#main-content')

  // Get hear this page url
  const hearThisIframe = content.querySelector('iframe[src*="hearthis.at/embed"]')
  const hearThisPageUrl = hearThisIframe
    ? hearThisIframe.getAttribute('src').replace('style=1&amp;', 'style=1/')
    : null

  // Get zippyshare page url
  let zippySharePageUrl = null
  const anchors = [...content.querySelectorAll('a[href*="zippyshare"]')]
  anchors.forEach(anchor => {
    if (zippySharePageUrl) return
    const href = anchor.getAttribute('href')
    if (href.includes('file.html')) {
      zippySharePageUrl = href
    }
  })

  const Entry = require('../../services/mongodb/models/Entry')

  const newEntry = new Entry({
    feed: feedId,
    custom: {
      ...item,
      content: content.innerHTML.replace(/\n/g, '').replace(/\t/g, ''),
      hearThisPageUrl,
      zippySharePageUrl,
      hearThisDownloadUrl: null,
      zippyShareDownloadUrl: null
    }
  })

  // Look for hear this download url
  getHearThisDownloadUrl(hearThisPageUrl)
    .then(audioSrc => {
      newEntry.custom.hearThisDownloadUrl = audioSrc
      if (audioSrc) {
        console.log(chalk.green(item.title), audioSrc)
        newEntry.scrapeSuccess = true
        return null
      }

      console.log(chalk.red('no hearthis download url'), item.title)

      // Look for zippyshare download url
      return getZippyShareDownloadUrl(zippySharePageUrl)
    })
    .then(audioSrc => {
      newEntry.custom.zippyShareDownloadUrl = audioSrc

      if (audioSrc) {
        console.log(chalk.green(item.title), audioSrc)
        newEntry.scrapeSuccess = true
        return
      }

      if (!newEntry.custom.hearThisDownloadUrl) {
        console.log(chalk.red('no zippyshare download url'), item.title)
        newEntry.scrapeError = true
      }

      return
    })
    .then(() => {
      const error = newEntry.validateSync()
      if (error) throw error
      return newEntry.save()
    })
    .then(resolve)
    .catch(err => {
      console.log(chalk.red(err), newEntry.title)
      resolve()
    })
})

const getHearThisDownloadUrl = hearThisPageUrl => new Promise(resolve => {
  if (!hearThisPageUrl) return resolve(null)
  console.log(chalk.cyan('[SCRAPE]'), hearThisPageUrl)
  const html = shell.exec(`curl ${hearThisPageUrl}`, { silent: true })
  const dom = new JSDOM(html)
  const player = dom.window.document.querySelector('[data-mp3]')
  if (!player) return resolve(null)
  const downloadUrl = player.getAttribute('data-mp3')
  return resolve(downloadUrl)
})

const getZippyShareDownloadUrl = zippySharePageUrl => new Promise(resolve => {
  if (!zippySharePageUrl) return resolve(null)
  console.log(chalk.cyan('[SCRAPE]'), zippySharePageUrl)
  const html = shell.exec(`curl ${zippySharePageUrl}`, { silent: true })
  const dom = new JSDOM(html, {
    runScripts: 'dangerously',
    virtualConsole: new VirtualConsole()
  })
  const dlbutton = dom.window.document.getElementById('dlbutton')
  if (!dlbutton) return resolve(null)
  const href = dlbutton.getAttribute('href')
  const host = zippySharePageUrl.split('.com/')[0] + '.com'
  const downloadUrl = host + href
  resolve(downloadUrl)
})
