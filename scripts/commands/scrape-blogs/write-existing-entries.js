const fs = require('fs')
const moment = require('moment')
const { JSDOM, VirtualConsole } = require('jsdom')
const shell = require('shelljs')
const chalk = require('chalk')
const feeds = require('../../data/blog-feeds')
const slugify = require('../../services/utils/slugify')

module.exports = () => new Promise(resolve => {
  console.log(chalk.gray('Writing existing entry slugs...'))

  const feedUrl = feeds[0]
  const Feed = require('../../services/mongodb/models/Feed')

  // Get feed then curl the first list page
  Feed.findOne({ feedUrl })
  .then()
  .then(getExistingSlugs)
  .then(slugs => {
    const SLUGS = __dirname + '/existing-slugs.json'
    console.log(chalk.gray('Writing file'), SLUGS)
    fs.writeFileSync(
      SLUGS,
      JSON.stringify(slugs.sort(), null, 2)
    )
    resolve()
  })
  .catch(err => {
    console.log(chalk.red(err))
    resolve()
  })
})

const getExistingSlugs = feed => {
  if (!feed) throw new Error('feed not found')
  const Entry = require('../../services/mongodb/models/Entry')
  return Entry.find({
    feed: feed._id,
    custom: { $ne: null }
  }, 'custom.slug').then(entries => {
    const slugs = entries.map(e => e.custom.slug)
    return slugs
  })
}
