const chalk = require('chalk')
const feeds = require('../../data/blog-feeds')

module.exports = (args = []) => {
  console.log(chalk.green('BLOGS'))
  feeds.map((feed, i) => console.log(chalk.yellow(i), feed))
  console.log()
}
