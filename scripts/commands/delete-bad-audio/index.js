const chalk = require('chalk')
const mongoose = require('mongoose')
mongoose.set('debug', true)
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')

module.exports = () => {
  connectDB('v1', MONGO_URL)
    .then(() => new Promise((resolve, reject) => {
      const Entry = require('../../services/mongodb/models/Entry')
      const Audio = require('../../services/mongodb/models/Audio')

      const FILESIZE_MIN = 1 * 1024 * 1024 // MB
      Audio.find({ size: { $lt: FILESIZE_MIN } })
        .then(audio => {
          console.log(chalk.gray(`Deleting ${chalk.white(audio.length)} records...`))

          return Promise.all(
            audio.map(a => {
              return Entry.findOne({ _id: a.entry })
                .then(entry => {
                  console.log(chalk.red('delete entry'), entry._id)
                  console.log(chalk.red('delete audio'), a._id)
                  return Promise.all([
                    entry.remove(),
                    a.remove()
                  ])
                })
            })
          )
        })
        .then(resolve)
    }))
    .then(() => process.exit())
    .catch(err => {
      console.log(chalk.red(err))
    })
}
