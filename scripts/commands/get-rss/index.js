const chalk = require('chalk')
const blogFeeds = require('../../data/blog-feeds')
const podcastFeeds = require('../../data/podcast-feeds')
const parseRssFeed = require('../../services/utils/parse-rss-feed')

module.exports = ([ type, index ]) => {
  let feedUrl = null
  if (type === 'blog') {
    feedUrl = blogFeeds[index] || null
  }
  if (type === 'podcast') {
    feedUrl = podcastFeeds[index] || null
  }

  if (!feedUrl) {
    console.log(chalk.red(`Feed URL not found for ${type} "${index}".`))
    console.log(chalk.gray(`Run ${chalk.white('nr e list-blogs')} to show all blog indexes.`))
    console.log(chalk.gray(`Run ${chalk.white('nr e list-podcasts')} to show all podcast indexes.`))
    console.log()
    process.exit()
  }

  parseRssFeed(feedUrl)
    .then(console.log)
}
