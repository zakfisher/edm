const shell = require('shelljs')
const { HOST, PORT, DB } = require('../../services/mongodb/credentials/local.json')

const ROOT_DIR = __dirname.split('/scripts')[0]
const DUMP_DIR = ROOT_DIR + '/google-drive/db'

module.exports = () => {
  shell.exec(`rm -rf ${DUMP_DIR}`)
  const dump = [
    'mongodump',
    `--host ${HOST}`,
    `--port ${PORT}`,
    `--db ${DB}`,
    `--out ${DUMP_DIR}`
  ].join(' ')
  shell.exec(dump)
}
