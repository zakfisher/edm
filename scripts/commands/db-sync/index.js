const shell = require('shelljs')
module.exports = ([ drop = false ]) => {
  shell.exec(`npm run exec db-dump`)
  shell.exec(`npm run exec db-deploy ${drop ? '--drop': ''}`)
}
