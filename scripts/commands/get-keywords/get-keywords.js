const fs = require('fs')
const chalk = require('chalk')
const shell = require('shelljs')
const _ = require('lodash')
const mongoose = require('mongoose')
mongoose.set('debug', true)
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')

const DATA_DIR = __dirname.split('/commands')[0] + '/data'
const KEYWORDS_DIR = DATA_DIR + '/keywords'
shell.exec(`rm -rf ${KEYWORDS_DIR} && mkdir -p ${KEYWORDS_DIR}`)

module.exports = () => {
  const Feed = require('../../services/mongodb/models/Feed')
  const Entry = require('../../services/mongodb/models/Entry')
  const Audio = require('../../services/mongodb/models/Audio')

  return Promise.all([
    Feed.find({ 'keywords.0': { $exists: true } }, 'keywords'),
    Entry.find({
      'custom.keywords.0': { $exists: true },
      scrapeSuccess: true
    }, 'custom.keywords'),
    Audio.find({ 'keywords.0': { $exists: true } }, 'keywords'),
  ])
    .then(([feeds, entries, audio]) => {
      const feedKeywords = getKeywords('feeds', feeds)
      const entryKeywords = getKeywords('entries', entries)
      const audioKeywords = getKeywords('audio', audio)
      const allKeywords = getAllKeywords([
        feedKeywords,
        entryKeywords,
        audioKeywords
      ])
      return
    })
}

const getAllKeywords = groups => {
  let allKeywords = []
  groups.map(kw => allKeywords = allKeywords.concat(kw))

  const keywords = Object.keys(
    allKeywords.reduce((obj, keyword) => {
      obj[keyword] = true
      return obj
    }, {})
  ).sort()

  writeTextFile('all', keywords)

  return keywords
}

const getKeywords = (name, collection) => {
  const keywords = Object.keys(
    collection.reduce((obj, doc) => {
      const words = _.get(doc, 'keywords', null)
        || _.get(doc, 'custom.keywords', null)
      words.map(keyword => {
        keyword.split(',').map(word => {
          obj[word.trim()] = true
        })
      })
      return obj
    }, {})
  ).sort()

  writeTextFile(name, keywords)

  return keywords
}

const writeTextFile = (name, keywords) => {
  console.log(chalk.green(name.toUpperCase()))
  console.log(keywords.length + ' keywords')

  const FILE_PATH = KEYWORDS_DIR + '/' + name + '.txt'

  fs.writeFileSync(
    FILE_PATH,
    keywords.join('\n')
  )

  console.log(FILE_PATH.split('edm')[1])
  console.log()
}