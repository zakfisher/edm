const chalk = require('chalk')
const shell = require('shelljs')
const mongoose = require('mongoose')
mongoose.set('debug', true)
const connectDB = require('../../services/mongodb/connect-db')
const { MONGO_URL } = require('../../services/mongodb/credentials/local.json')
const getKeywords = require('./get-keywords')

module.exports = ([ index ]) => {
  connectDB('v1', MONGO_URL)
    .then(getKeywords)
    .then(() => process.exit())
    .catch(err => {
      console.log(chalk.red(err))
    })
}
