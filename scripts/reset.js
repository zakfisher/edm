const imagekit = require('./services/imagekit/server')
const backblaze = require('./services/backblaze/server')
const child_process = require('child_process')

Promise.all([
  imagekit.listUploadedMediaFiles(0, 1000),
  backblaze.listFiles('edm-town')
])
  .then(([ikRes, b2Res]) => {
    const removeImageKitFiles = () => Promise.all(ikRes.map(file => {
      return imagekit.deleteFile(file.itemPath)
    }))

    const removeBackBlazeFiles = () => Promise.all(b2Res.files.map(file => {
      return backblaze.deleteFile(file.fileId)
    }))

    return Promise.all([
      removeImageKitFiles(),
      removeBackBlazeFiles()
    ])
  })
  .then(([ikRes, b2Res]) => {
    console.log(`Deleted ${ikRes.length} files from ImageKit.`)
    console.log(`Deleted ${b2Res.length} files from BackBlaze.`)
    // child_process.execFileSync('firebase', ['firestore:delete', '--all-collections'], { stdio: 'inherit' })
  })
  .catch(err => {
    console.error(err)
  })
