const axios = require('axios')
const chalk = require('chalk')
const { FEEDLY_API, FEEDLY_TOKEN } = require('./credentials/server.json')

const server = {}

const api = axios.create({ baseURL: FEEDLY_API })

server.getStream = (feedId, count = 1000, continuation) => {
  let endpoint = `/streams/${encodeURIComponent(`feed/${feedId}`)}/ids?count=${count}`
  if (continuation) endpoint += `&continuation=${continuation}`
  return api.get(endpoint)
}

server.getEntries = entryIds => {
  if (!entryIds || !entryIds.length) return null
  return api.post('/entries/.mget', entryIds)
}

server.getFeedHistory = feedUrl => {
  console.log(chalk.cyan('[FETCH ENTRIES]'), feedUrl)
  return new Promise((resolve, reject) => {

    let entries = []
    let continuation = null

    // Import Feedly entries, 1000 at a time
    const getNextBatch = () => {
      server.getStream(feedUrl, 1000, continuation)
        .then(res => {
          continuation = res.data.continuation || null
          return server.getEntries(res.data.ids)
        })
        .then(res => {
          entries = entries.concat(res.data)
          if (continuation) return getNextBatch()
          resolve(entries)
        })
        .catch(reject)
    }

    getNextBatch()
  })
}

module.exports = server
