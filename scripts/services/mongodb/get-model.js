const fs = require('fs')

module.exports = model => {
  const path = `${__dirname}/models/${model}`
  if (!fs.existsSync(path)) {
    throw new Error(`Model "${model}" not found`)
  }
  return require(path)
}
