const { Schema } = require('mongoose')

const AudioSchema = new Schema({
  feed: {
    type: Schema.Types.ObjectId,
    ref: 'Feed',
    required: true
  },
  entry: {
    type: Schema.Types.ObjectId,
    ref: 'Entry',
    required: true
  },
  event: {
    type: Schema.Types.ObjectId,
    ref: 'Event',
  },
  slug: {
    type: String,
    default: null
  },
  title: {
    type: String,
    default: null
  },
  date: {
    type: Date,
    default: null
  },
  summary: {
    type: String,
    default: null
  },
  website: {
    type: String,
    default: null
  },
  keywords: {
    type: Array,
    default: []
  },
  audio: {
    type: String,
    default: null
  },
  duration: {
    type: Number,
    default: null
  },
  size: {
    type: Number,
    default: null
  },
}, { timestamps: true })

module.exports = AudioSchema
