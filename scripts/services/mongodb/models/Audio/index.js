const mongoose = require('mongoose')
const { getDB } = require('../../dbs')
const Schema = require('./schema')

// Add indexes
Schema.index({ feed: 1 })
Schema.index({ date: -1 })

// Add statics (collection)
Schema.statics.getById = require('./statics/get-by-id')

// Create model
const db = getDB('v1')
const Audio = db.model('Audio', Schema)
Audio.createIndexes()

module.exports = Audio
