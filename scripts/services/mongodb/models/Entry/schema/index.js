const { Schema } = require('mongoose')

const EntrySchema = new Schema({
  feed: {
    type: Schema.Types.ObjectId,
    ref: 'Feed',
    required: true
  },
  feedly: {
    type: Object,
    default: null
  },
  rss: {
    type: Object,
    default: null
  },
  custom: {
    type: Object,
    default: null
  },
  scrapeSuccess: {
    type: Boolean,
    default: false
  },
  scrapeError: {
    type: Boolean,
    default: false
  },
  downloadSuccess: {
    type: Boolean,
    default: false
  },
  downloadError: {
    type: Boolean,
    default: false
  },
  uploadSuccess: {
    type: Boolean,
    default: false
  },
  uploadError: {
    type: Boolean,
    default: false
  },
}, { timestamps: true })

module.exports = EntrySchema
