const mongoose = require('mongoose')
const { getDB } = require('../../dbs')
const Schema = require('./schema')

// Add indexes
Schema.index({ feed: 1 })
Schema.index({ createdAt: 1 })
Schema.index({ scrapeSuccess: -1 })
Schema.index({ scrapeError: -1 })
Schema.index({ downloadSuccess: -1 })
Schema.index({ downloadError: -1 })
Schema.index({ uploadSuccess: -1 })
Schema.index({ uploadError: -1 })

// Add statics (collection)
Schema.statics.getById = require('./statics/get-by-id')

// Create model
const db = getDB('v1')
const Entry = db.model('Entry', Schema)
Entry.createIndexes()

module.exports = Entry
