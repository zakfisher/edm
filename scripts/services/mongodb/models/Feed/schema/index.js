const { Schema } = require('mongoose')

const FeedSchema = new Schema({
  __t: {
    required: true,
    type: String,
    enum: ['PODCAST', 'BLOG']
  },
  feedUrl: {
    required: true,
    type: String
  },
  slug: {
    type: String,
    default: null
  },
  title: {
    type: String,
    default: null
  },
  website: {
    type: String,
    default: null
  },
  description: {
    type: String,
    default: null
  },
  image: {
    type: Object,
    default: null
  },
  lastUpdated: {
    type: Date,
    default: null
  },
  lastFetched: {
    type: Date,
    default: null
  },
  rss: {
    type: Object,
    default: null
  },
  keywords: {
    type: Array,
    default: []
  },
  continuation: {
    type: String,
    default: null
  }
}, { timestamps: true })

module.exports = FeedSchema
