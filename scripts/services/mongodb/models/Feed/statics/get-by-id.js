const mongoose = require('mongoose')

module.exports = function getById(id, fields = {}, populateOpts = '') {
  return new Promise((resolve, reject) => {
    // Make sure it's a valid ObjectId before querying
    if (!mongoose.Types.ObjectId.isValid(id)) return reject({ message: `Invalid ObjectId: "${id}"`})

    return resolve(this.findOne({ _id: id.toString() }, fields).populate(populateOpts))
  })
}
