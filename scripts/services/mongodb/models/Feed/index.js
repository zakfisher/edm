const mongoose = require('mongoose')
const { getDB } = require('../../dbs')
const Schema = require('./schema')

// Add indexes
Schema.index({ feedUrl: 1 })
Schema.index({ slug: 1 })
Schema.index({ title: 1 })

// Add statics (collection)
Schema.statics.getById = require('./statics/get-by-id')

// Create model
const db = getDB('v1')
const Feed = db.model('Feed', Schema)
Feed.createIndexes()

module.exports = Feed
