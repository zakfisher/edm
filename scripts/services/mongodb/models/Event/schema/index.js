const { Schema } = require('mongoose')

const FeedSchema = new Schema({
  __t: {
    type: String,
    enum: [
      'CLUB',
      'FESTIVAL',
      'RADIO',
      'OTHER'
    ],
    default: 'FESTIVAL'
  },
  slug: {
    required: true,
    type: String,
    default: null
  },
  title: {
    required: true,
    type: String,
    default: null
  },
  website: {
    type: String,
    default: null
  },
  description: {
    type: String,
    default: null
  },
  image: {
    type: Object,
    default: null
  },
  keywords: {
    type: Array,
    default: []
  },
  startDate: {
    type: Date,
    default: null
  },
  endDate: {
    type: Date,
    default: null
  },
}, { timestamps: true })

module.exports = FeedSchema
