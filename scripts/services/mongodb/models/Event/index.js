const mongoose = require('mongoose')
const { getDB } = require('../../dbs')
const Schema = require('./schema')

// Add indexes
Schema.index({ slug: 1 })
Schema.index({ title: 1 })

// Add statics (collection)
Schema.statics.getById = require('./statics/get-by-id')

// Create model
const db = getDB('v1')
const Event = db.model('Event', Schema)
Event.createIndexes()

module.exports = Event
