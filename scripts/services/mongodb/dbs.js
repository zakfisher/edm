const dbs = {}

exports.getDB = key => dbs[key]

exports.setDB = (key, db) => dbs[key] = db

