const fs = require('fs')
const chalk = require('chalk')
const mongoose = require('mongoose')
const { Mongoose } = mongoose
const { setDB } = require('./dbs')

const MODELS = fs.readdirSync(`${__dirname}/models`).filter(n => n[0] !== '.')

const connectDB = (key, url) => new Promise((resolve, reject) => {
  if (!url) return reject('No Mongo URL found')

  const db = new Mongoose()
  setDB(key, db)

  db.connect(url, {
    autoIndex: false,
    useNewUrlParser: true
  })

  if (process.env.NODE_ENV === 'test') return resolve(db)

  db.connection.once('open', () => {
    MODELS.map(model => require(`./models/${model}`))
    if (process.env.NODE_ENV === 'dev') {
      return resolve(db)
    }
    console.log('MongoDB Connection')
    console.log(chalk.magenta(url), '\n')
    return resolve(db)
  })

  db.connection.on('error', (e) => {
    console.log('MongoDB Error')
    console.log(chalk.red(e), '\n')
    return reject()
  })

  process.on('SIGINT', () => {
    try {
      console.log('\nMongo Connection Closed')
      console.log(chalk.red(url), '\n')
      db.disconnect()
    }
    catch (e) {
      // do nothing
    }
    finally {
      process.exit(0)
    }
  })
})

module.exports = connectDB
