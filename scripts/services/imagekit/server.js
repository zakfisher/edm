const ImageKit = require('imagekit')
const credentials = require('./credentials/server.json')

const imagekit = new ImageKit(credentials)

module.exports = imagekit
