const firebase = require('firebase/app')
require('firebase/firestore')
const config = require('./credentials/client.json')

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

module.exports = firebase