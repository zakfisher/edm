const admin = require('firebase-admin')

const serviceAccount = require('./credentials/server.json')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://edm-town.firebaseio.com'
})

module.exports = admin
