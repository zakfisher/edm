const axios = require('axios')
const chalk = require('chalk')
const shell = require('shelljs')
const child_process = require('child_process')
const { applicationKeyId, applicationKey, bucketName } = require('./credentials/server.json')

const server = {}

server.authorize = () => {
  return new Promise((resolve, reject) => {
    const child = shell.exec(`b2 authorize-account ${applicationKeyId} ${applicationKey}`, { silent: true })
    if (child.stderr) return reject(child.stderr)
    resolve(child.stdout)
  })
}

server.getDownloadAuth = () => {
  return new Promise((resolve, reject) => {
    const child = shell.exec(`b2 get-download-auth ${bucketName}`, { silent: true })
    if (child.stderr) return reject(child.stderr)
    resolve(child.stdout)
  })
}

server.createBucket = (bucketName, private = true) => {
  return new Promise((resolve, reject) => {
    const child = shell.exec(`b2 create-bucket ${bucketName} ${private ? 'allPrivate' : 'allPublic'}`, { silent: true })
    if (child.stderr) return reject(child.stderr)
    resolve(child.stdout)
  })
}

server.deleteBucket = bucketName => {
  return new Promise((resolve, reject) => {
    const child = shell.exec(`b2 delete-bucket ${bucketName}`, { silent: true })
    if (child.stderr) return reject(child.stderr)
    resolve(child.stdout)
  })
}

server.listFiles = bucketName => {
  return new Promise((resolve, reject) => {
    const child = shell.exec(`b2 list-file-versions ${bucketName}`, { silent: true })
    if (child.stderr) return reject(child.stderr)
    resolve(JSON.parse(child.stdout))
  })
}

server.deleteFile = fileId => {
  return new Promise((resolve, reject) => {
    const child = shell.exec(`b2 delete-file-version ${fileId}`, { silent: true })
    if (child.stderr) return reject(child.stderr)
    resolve(child.stdout)
  })
}

server.uploadFile = (src, dest) => {
  dest = dest.split('?')[0]
  return new Promise((resolve, reject) => {
    const child = child_process.execFileSync('b2', ['upload-file', bucketName, src, dest], { stdio: 'pipe' })
    let output = child.toString()
    if (!output.includes('fileId')) return reject(output)
    output = output.split('\n')
    const url = output[0].split('URL by file name: ')[1]
    resolve(url)
  })
}

server.authorize()
module.exports = server
