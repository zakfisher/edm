const _ = require('lodash')
const axios = require('axios')
const { applicationKeyId, applicationKey, bucketName } = require('./credentials/server.json')

const server = {}

const api = axios.create({
  baseURL: 'https://api.backblazeb2.com/b2api/v2',
  headers: {
    Authorization: `Basic ${Buffer.from(applicationKeyId + ':' + applicationKey).toString('base64')}`
  }
})

server.authorize = () => new Promise((resolve, reject) => {
  api.get('/b2_authorize_account')
    .then(res => resolve(_.get(res, 'data', null)))
    .catch(reject)
})

module.exports = server
