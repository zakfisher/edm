/* eslint-disable no-console */
const fs = require('fs')
// const _ = require('lodash')
const chalk = require('chalk')
const shell = require('shelljs')
const request = require('request')
const { parseString } = require('xml2js')

const parseRssFeed = feedUrl => {
  console.log(chalk.cyan('[FETCH RSS]'), feedUrl)
  return new Promise((resolve, reject) => {

    // CURL the feed for raw XML
    request(feedUrl, function (err, res, rss) {
      if (err) return reject(err)

      // Curate XML to make sure we can read it properly
      rss = rss.replace(/\&amp\;/g, '~:|:~')
      rss = rss.replace(/\&/g, '&amp;')
      rss = rss.replace(/\~\:\|\:\~/g, '&amp;')

      // Convert
      const opts = {
        explicitArray: false,
        mergeAttrs: true,
        trim: true
      }

      parseString(rss, opts, (err, result) => {
        if (err) return reject(err)
        const { channel } = result.rss
        resolve(channel)
      })

    })
  })
}

module.exports = parseRssFeed

