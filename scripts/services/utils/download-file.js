const MultipartDownload = require('multipart-download')
const fs = require('fs')
const chalk = require('chalk')
const shell = require('shelljs')

require('events').EventEmitter.defaultMaxListeners = 15

const downloadFile = (src, dest, onData = null) => new Promise(resolve => {

  const startDownload = () => {
    const dl = new MultipartDownload()

    dl.start(src, {
      numOfConnections: 5,
      writeToBuffer: true,
      saveDirectory: dest,
    })

    if (onData) {
      dl.on('data', (data, offset) => onData(data, offset))
    }

    dl.on('end', resolve)
    dl.on('error', err => {
      console.log(chalk.red(err), src)
      resolve()
    })
  }

  if (fs.existsSync(dest)) return startDownload()

  shell.exec(`mkdir ${dest}`, () => {
    setTimeout(startDownload, 1000)
  })
})

module.exports = downloadFile
