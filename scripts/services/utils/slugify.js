const slug = require('slug')
const decodeEntities = require('./decode-entities')

const slugify = str => slug(decodeEntities(str), {
  "replacement": "_",
  "lower": true
})

module.exports = slugify
