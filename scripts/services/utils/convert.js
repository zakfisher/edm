const converters = {
  day: {
    hr: day => day * 24,
    min: day => day * 24 * 60,
    sec: day => day * 24 * 60 * 60
  },
  hr: {
    day: hr => hr / 24,
    min: hr => hr * 60,
    sec: hr => hr * 60 * 60
  },
  min: {
    day: min => min / 60 / 24,
    hr: min => min / 60,
    sec: min => min * 60
  },
  sec: {
    day: sec => sec / 60 / 60 / 24,
    hr: sec => sec / 60 / 60,
    min: sec => sec / 60
  }
}

module.exports = (val, from, to) => converters[from][to](val)
