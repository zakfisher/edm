const { JSDOM } = require('jsdom')

const dom = new JSDOM('')

const decodeEntities = str => {
  dom.window.document.body.innerHTML = `<p>${str}</p>`
  return dom.window.document.querySelector('p').textContent
}

module.exports = decodeEntities
