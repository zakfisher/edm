const functions = require('firebase-functions')
const app = require('./server/serve-firebase')
exports.app = functions.https.onRequest(app)
